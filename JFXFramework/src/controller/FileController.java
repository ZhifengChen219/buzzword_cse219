package controller;

import java.io.IOException;

/**
 * @author Ritwik Banerjee
 */
public interface FileController {


    void handleCloseRequest();
    void handleCreateNewProfileRequest(String username, String password);
    void handleLoginRequest(String profileName, String password);
    void createLoginDialog();
    void handleLogOutRequest();
    void handleStartPlayingRequest();
    void handleSelectModeRequest(int selectedLevel);
    void handleHomeButtonRequest();
    void handlePauseButtonRequest();
    void handleResumeButtonRequest();
    void handleCreateButtonRequest(String username, String password);
    void handleLevelButtonRequest();
    void handleConfirmRequest(String username, String password);
    void handleModeComboBoxRequest();
    void handleHelpRequest();
    void handleExitRequest();
    void handleMouseDragRequest();
    void handleTypeRequest();
    void selectAndDisplayWord();
    void handlePlayNextButton();
    void handleSaveButton();
    void initData();
}
