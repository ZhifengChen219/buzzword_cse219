package ui;

import apptemplate.AppTemplate;
import components.AppStyleArbiter;
import controller.FileController;
import gui.HomeScreen;
import gui.Workspace;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static buzzword.BuzzwordProperties.HEADING_LABEL;
import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;


/**
 * This class provides the basic user interface for this application, including all the file controls, but it does not
 * include the workspace, which should be customizable and application dependent.
 *
 * @author Richard McKenna, Ritwik Banerjee
 */
public class AppGUI implements AppStyleArbiter {
    private final PropertyManager propertyManager = PropertyManager.getManager();
    final KeyCombination keyComb1 = new KeyCodeCombination(KeyCode.L,
            KeyCombination.CONTROL_DOWN);
    protected FileController fileController;   // to react to file-related controls
    protected Stage          primaryStage;     // the application window
    protected Scene          primaryScene;     // the scene graph
    private AppTemplate app;
    private String applicationTitle;
    private int windowHeight;
    private int windowWidth;
    private Workspace workspace;
    private VBox outerpane;
    private HBox headPane;
    private Button closeButton;

    private BorderPane innerPane;
    private Label guiHeadingLabel;



    public AppGUI(AppTemplate appTemplate, Stage primaryStage, String applicationTitle, int windowHeight, int windowWidth) {
        this.app = appTemplate;
        this.primaryStage = primaryStage;
        this.applicationTitle = applicationTitle;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
        try {
            Method         getFileControllerClassMethod = app.getClass().getMethod("getFileControllerClass");
            String         fileControllerClassName      = (String) getFileControllerClassMethod.invoke(app);
            Class<?>       klass                        = Class.forName("controller." + fileControllerClassName);
            Constructor<?> constructor                  = klass.getConstructor(AppTemplate.class);
            fileController = (FileController) constructor.newInstance(app);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException |
                InstantiationException e) {
            e.printStackTrace();
            System.exit(1);
        }

    }

    public void initScene() throws IOException{
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        HomeScreen homeScreen = (HomeScreen) workspace.getScreen(
                propertyManager.getPropertyValue(HOMESCREEN));
        outerpane = new VBox();
        createHeadPane();

        workspace = (Workspace) app.getWorkspaceComponent();
        primaryStage.setTitle(applicationTitle);
        innerPane = workspace.getScreen(propertyManager.getPropertyValue(HOMESCREEN)).getBorderPane();

        outerpane.getChildren().addAll(headPane, innerPane);
        outerpane.setStyle("-fx-background-color: #A0A0A0;");

        primaryScene = new Scene(outerpane, Integer.parseInt(propertyManager.getPropertyValue(APP_WINDOW_WIDTH)),
                Integer.parseInt(propertyManager.getPropertyValue(APP_WINDOW_HEIGHT)));


        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resrouces folder does not exist.");
        try (InputStream appLogoStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(APP_LOGO)))) {
            primaryStage.getIcons().add(new Image(appLogoStream));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        primaryScene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent event) {
                        if (keyComb1.match(event)) {
                            homeScreen.loginButtonSetonActon();
                            event.consume();
                        }
                    }
                });
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    public void setInnerPane(BorderPane borderPane) {
        innerPane = borderPane;
        outerpane.getChildren().remove(1);
        outerpane.getChildren().add(borderPane);
    }

    public void createHeadPane() throws IOException{
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(APP_TITLE));
        closeButton = initializeButton(CLOSE_ICON.toString(), null);
        closeButton.setStyle("-fx-background-color: #A0A0A0");
        headPane = new HBox();

        HBox innerHbox1 = new HBox();
        innerHbox1.getChildren().addAll(guiHeadingLabel);
        innerHbox1.setPadding(new Insets(0, 240, 0, 300));
        HBox innerHbox2 = new HBox();
        innerHbox2.getChildren().addAll(closeButton);
        closeButton.setOnAction(e-> fileController.handleCloseRequest());
        headPane.getChildren().addAll(innerHbox1, innerHbox2);
        headPane.setAlignment(Pos.BASELINE_CENTER);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));
    }

    @Override
    public void initStyle() {
        // currently, we do not provide any stylization at the framework-level
    }
    public FileController getFileController() {
        return this.fileController;
    }

    public Scene getPrimaryScene() {
        return primaryScene;
    }

    public Button initializeButton(String icon, String name) throws IOException {
        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");
        Button button = new Button(name);
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            ImageView imageView = new ImageView(buttonImage);
            imageView.setFitHeight(20);
            imageView.setFitWidth(20);
            button.setGraphic(imageView);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return button;
    }
}
