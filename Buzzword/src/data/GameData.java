package data;

import java.io.Serializable;

/**
 * Created by Archi on 11/6/16.
 */
public class GameData  implements Serializable{
    private String profileName;
    private String password;
    private int animalsLevel = 1;
    private int sportsLeveL = 1;
    private int restaurantLevel = 1;

    public GameData(String profileName, String password) {
        this.profileName = profileName;
        this.password = password;
    }

    public GameData(){
    }

    public int getRestaurantLevel() {
        return restaurantLevel;
    }

    public void setRestaurantLevel(int restaurantLevel) {
        this.restaurantLevel = restaurantLevel;
    }

    public int getAnimalsLevel() {
        return animalsLevel;
    }

    public void setAnimalsLevel(int animalsLevel) {
        this.animalsLevel = animalsLevel;
    }

    public int getSportsLeveL() {
        return sportsLeveL;
    }

    public void setSportsLeveL(int sportsLeveL) {
        this.sportsLeveL = sportsLeveL;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfileName() {
        return profileName;
    }

    public String getPassword() {
        return password;
    }

    public int getModeLevel(String value) {
       if(value.equals("Sports")) {
            return sportsLeveL;
        }else if(value.equals("Restaurant")){
            return restaurantLevel;
        }else if(value.equals("Animals")) {
            return animalsLevel;
        }else {
            return -1;
        }

    }
}
