package data;

import components.AppDataComponent;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Archi on 11/19/16.
 */
public class GameDataHug implements Serializable, AppDataComponent {
    private static GameDataHug singleton;

    private Map<String, GameData> gamedataHugMap;

    public GameDataHug() {
        gamedataHugMap = new HashMap<>();
    }

    public static GameDataHug getSingleton() {
        if (singleton == null)
            singleton = new GameDataHug();
        return singleton;
    }

    public void addGameData(String profileName, GameData gameData) {
        gamedataHugMap.put(profileName, gameData);
    }

    public Map getGameDataHugMap() {
        return gamedataHugMap;
    }

    public void setGamedataHugMap(HashMap hashMap){
        this.gamedataHugMap = hashMap;
    }

    @Override
    public void reset() {
        this.gamedataHugMap = new HashMap<>();
    }
}
