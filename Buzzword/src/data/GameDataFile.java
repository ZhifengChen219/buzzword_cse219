package data;

import components.AppDataComponent;
import components.AppFileComponent;

import java.io.*;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Archi on 11/6/16.
 */
public class GameDataFile implements AppFileComponent {
    FileOutputStream fos = null;
    ObjectOutputStream oos = null;
    FileInputStream fis = null;
    ObjectInputStream ois = null;


    @Override
    public void saveData(AppDataComponent data) {
        GameDataHug gameDataHug = (GameDataHug) data;
        try {
            fos = new FileOutputStream("/Users/Archi/Desktop/BuzzwordGame3/saved/savedData.dat");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(gameDataHug.getGameDataHugMap());
            oos.close();
            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //

    @Override
    public Map loadData(AppDataComponent data) throws IOException, ClassNotFoundException{
        fis = new FileInputStream("/Users/Archi/Desktop/BuzzwordGame3/saved/savedData.dat");
        try {
            ois = new ObjectInputStream(fis);
            HashMap hashMap = (HashMap) ois.readObject();
            ois.close();
            return hashMap;
        }catch (EOFException e) {
            return null;
        }

    }

    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException {

    }
}
