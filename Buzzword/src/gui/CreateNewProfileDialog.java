package gui;

import apptemplate.AppTemplate;
import controller.BuzzwordController;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;

/**
 * Created by Archi on 11/17/16.
 */
public class CreateNewProfileDialog extends Stage{
    private static CreateNewProfileDialog singleton;
    private VBox outerVbox;
    private Scene scene;

    private HBox hbox1;
    private Label profileLabel;
    private TextField profileTextField;

    private HBox hbox2;
    private Label passwordLabel;
    private PasswordField passwordField;

    private HBox hbox3;
    private Label confirmPasswordLabel;
    private PasswordField confirmPasswordField;

    private AppTemplate appTemplate;
    private AppGUI gui;
    private BuzzwordController controller;

    public static CreateNewProfileDialog getSingleton() {
        if (singleton == null)
            singleton = new CreateNewProfileDialog();
        return singleton;
    }

    public void init(Stage primaryStage) {
        AppMessageDialogSingleton appMessageDialogSingleton = AppMessageDialogSingleton.getSingleton();
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        outerVbox = new VBox();
        hbox1 = new HBox();
        profileLabel = new Label("Enter Profile Name: ");
        profileTextField = new TextField();
        hbox1.getChildren().addAll(profileLabel, profileTextField);
        hbox1.setSpacing(11);

        hbox2 = new HBox();
        passwordLabel = new Label("Enter Password: ");
        passwordField = new PasswordField();
        hbox2.getChildren().addAll(passwordLabel, passwordField);
        hbox2.setSpacing(30);

        hbox3 = new HBox();
        confirmPasswordLabel = new Label("Confirm Password: ");
        confirmPasswordField = new PasswordField();
        hbox3.getChildren().addAll(confirmPasswordLabel, confirmPasswordField);
        hbox3.setSpacing(14);

        outerVbox.getChildren().addAll(hbox1, hbox2, hbox3);
        outerVbox.setSpacing(8);
        outerVbox.setPadding(new Insets(10,10,10,10));
        outerVbox.setStyle("-fx-background-color: #E0E0E0;");
        scene = new Scene(outerVbox);
        this.setScene(scene);
        this.initStyle(StageStyle.TRANSPARENT);

        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent ke) {
                if (ke.getCode() == KeyCode.ESCAPE) {
                    close();
                } else if(ke.getCode() == KeyCode.ENTER) {
                    gui = appTemplate.getGUI();
                    controller = (BuzzwordController) gui.getFileController();

                    if(profileTextField.getText().isEmpty()){
                        appMessageDialogSingleton.show(null, "Your profile name is empty");

                    } else if(passwordField.getText().isEmpty()){
                        appMessageDialogSingleton.show(null, "Your password field is empty");
                    }else if(confirmPasswordField.getText().isEmpty()){
                        appMessageDialogSingleton.show(null, "Your confirm password field is empty");
                    } else if(!passwordField.getText().equals(confirmPasswordField.getText())){
                        appMessageDialogSingleton.show(null, "Your passwords don't match.");
                        clearPasswordTextField();
                        clearConfirmPasswordTextField();
                    } else {
                        controller.handleCreateNewProfileRequest(profileTextField.getText(), passwordField.getText());
                    }

                }
            }
        });

    }

    public void setAppTemplate(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void clearProfileNameTextField() {
        profileTextField.clear();
    }

    public void clearPasswordTextField(){
        passwordField.clear();
    }

    public void clearConfirmPasswordTextField() {
        confirmPasswordField.clear();
    }

}
