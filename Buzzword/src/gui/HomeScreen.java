package gui;

import apptemplate.AppTemplate;
import controller.BuzzwordController;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import propertymanager.PropertyManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static buzzword.BuzzwordProperties.*;
import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * Created by Archi on 11/6/16.
 */
public class HomeScreen implements Screen {
    private  final PropertyManager propertyManager = PropertyManager.getManager();
    private BuzzwordController controller;

    private HBox headPane;
    private Label guiHeadingLabel;

    private BorderPane homeScreenborderPane;
    private Button createNewButton;
    private Button loginButton;
    private VBox vbox1;

    private Button logoutButton;
    private Button viewprofileButton;
    private ComboBox selectMode;
    private Button startPlaying;
    private Button helpButton;
    private VBox vbox2;


    private VBox circlesBox;
    private HBox hcircleBox1;
    private HBox hcircleBox2;
    private HBox hcircleBox3;
    private HBox hcircleBox4;
    private Button[] circles;

    public HomeScreen(BuzzwordController controller) throws IOException{
        this.controller = controller;
        homeScreenborderPane = new BorderPane();
        createLeftVBox1();
        createLeftVBox2();
        createCirclesBox();
        setLeftVBox1();
        homeScreenborderPane.setCenter(circlesBox);
        loginButton.setOnAction(e->controller.createLoginDialog());
        createNewButton.setOnAction(e->controller.createNewProfileDialog());
        viewprofileButton.setOnAction(e->controller.createProfileSettingScreen());
    }

    public void loginButtonSetonActon() {
        controller.createLoginDialog();
    }

    @Override
    public BorderPane getBorderPane() {
        return homeScreenborderPane;
    }


    public void createLeftVBox1() throws IOException{
        createNewButton = initializeButton(PROFILE_ICON.toString(), "Create New Profile", false);
        loginButton = initializeButton(LOGIN_ICON.toString(), "Login",false);
        createNewButton.setMaxWidth(Double.MAX_VALUE);
        loginButton.setMaxWidth(Double.MAX_VALUE);
        vbox1 = new VBox();
        vbox1.setSpacing(15);
        vbox1.setPadding(new Insets(50,0,0,20));
        vbox1.getChildren().addAll(createNewButton, loginButton);

    }

    public void createLeftVBox2() throws IOException{
        logoutButton = initializeButton(LOGOUT_ICON.toString(), "Log out", false);
        viewprofileButton = initializeButton(PROFILE_ICON.toString(), "View Profile", false);
        helpButton = initializeButton(PROFILE_ICON.toString(), "Help", false);
        createCombobox();
        setComboboxDisabled(false);
        startPlaying = initializeButton(PLAY_ICON.toString(), "Start Playing", true);
        logoutButton.setMaxWidth(Double.MAX_VALUE);
        selectMode.setMaxWidth(Double.MAX_VALUE);
        startPlaying.setMaxWidth(Double.MAX_VALUE);
        viewprofileButton.setMaxWidth(Double.MAX_VALUE);
        helpButton.setMaxWidth(Double.MAX_VALUE);
        vbox2 = new VBox();
        vbox2.setSpacing(15);
        vbox2.setPadding(new Insets(50,0,0,50));
        vbox2.getChildren().addAll(logoutButton, viewprofileButton, helpButton, selectMode, startPlaying);
        logoutButton.setOnAction(e->controller.handleLogOutRequest());
        selectMode.setOnAction(e->controller.createLevelSelectionScreen(selectMode.getValue().toString()));
        startPlaying.setOnAction(e->{
            controller.createGamePlayScreen();
        });
        helpButton.setOnAction(e->controller.handleHelpRequest());
    }

    public void setLeftVBox1() {
        homeScreenborderPane.setLeft(vbox1);
    }

    public void setLeftVBox2() {
        homeScreenborderPane.setLeft(vbox2);
    }

    public void createCombobox() throws FileNotFoundException{
        selectMode = new ComboBox();
        selectMode.getItems().addAll(propertyManager.getPropertyValue(ANIMALS),
                                    propertyManager.getPropertyValue(SPORTS),
                                    propertyManager.getPropertyValue(RESTAURANT));
        selectMode.setValue("Select Mode");
        selectMode.setStyle("-fx-base: #C0C0C0;");

    }

    public void setDefaultSelectModePromptText(){
        selectMode.setValue("Select Mode");
    }

    public void setComboboxDisabled(boolean disabled) {
        selectMode.setDisable(disabled);
    }

    public void setStartPlayingDisabled(boolean disabled) {
        startPlaying.setDisable(disabled);
    }

    public void setCreateNewButtonDisabled(boolean disabled) {
        createNewButton.setDisable(disabled);
    }

    public void setLoginButtonDisabled(boolean disabled) {
        loginButton.setDisable(disabled);
    }


    public void createCirclesBox() {
        circlesBox = new VBox();
        hcircleBox1 = new HBox();
        hcircleBox2 = new HBox();
        hcircleBox3 = new HBox();
        hcircleBox4 = new HBox();
        double r = 25;
        circles = new Button[16];
        for(int i = 0; i < 16; i++){
            circles[i] = new Button();
            circles[i].setShape(new Circle(r));
            circles[i].setMinSize(2*r, 2*r);
            circles[i].setMaxSize(2*r, 2*r);
            circles[i].setStyle("-fx-base: #606060;");
            circles[i].setDisable(true);
        }
        for(int i = 0; i < 4; i++) {
            hcircleBox1.getChildren().add(circles[i]);
        }
        for(int i = 4; i < 8; i++) {
            hcircleBox2.getChildren().add(circles[i]);
        }
        for(int i = 8; i < 12; i++) {
            hcircleBox3.getChildren().add(circles[i]);
        }
        for(int i = 12; i < 16; i++) {
            hcircleBox4.getChildren().add(circles[i]);
        }

        circles[0].setText("B");
        circles[1].setText("U");
        circles[4].setText("Z");
        circles[5].setText("Z");
        circles[10].setText("W");
        circles[11].setText("O");
        circles[14].setText("R");
        circles[15].setText("D");

        hcircleBox1.setSpacing(40);
        hcircleBox2.setSpacing(40);
        hcircleBox3.setSpacing(40);
        hcircleBox4.setSpacing(40);
        circlesBox.getChildren().addAll(hcircleBox1, hcircleBox2, hcircleBox3, hcircleBox4);
        circlesBox.setSpacing(30);
        circlesBox.setPadding(new Insets(80, 0, 0, 80));
    }

    public Button initializeButton(String icon, String name, boolean disabled) throws IOException {
        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");
        Button button = new Button(name);
        button.setDisable(disabled);
        button.setStyle("-fx-base: #C0C0C0;");
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            ImageView imageView = new ImageView(buttonImage);
            imageView.setFitHeight(18);
            imageView.setFitWidth(18);
            button.setGraphic(imageView);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return button;
    }
}
