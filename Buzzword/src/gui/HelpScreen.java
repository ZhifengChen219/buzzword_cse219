package gui;

import apptemplate.AppTemplate;
import controller.BuzzwordController;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import propertymanager.PropertyManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static settings.AppPropertyType.HOME_ICON;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * Created by Archi on 12/11/16.
 */
public class HelpScreen implements Screen {
    private final PropertyManager propertyManager = PropertyManager.getManager();
    private BuzzwordController controller;

    private BorderPane helpScreenBoderPane;

    private VBox leftPane;
    private Button homeButton;

    private ScrollPane scrollPane;


    public HelpScreen(BuzzwordController controller) throws IOException {
        this.controller = controller;
        helpScreenBoderPane = new BorderPane();
        createLeftPane();
        scrollPane = new ScrollPane();
        VBox vBox = new VBox();
        Label label = new Label("Help:\n" +
                "This is the help screen");
        TextArea textArea = new TextArea();
        vBox.getChildren().addAll(label);
        vBox.setStyle("-fx-font-size: 10pt;");

        scrollPane.setContent(vBox);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setStyle("-fx-background-color: #E0E0E0");
        scrollPane.setPrefSize(100,450);
        helpScreenBoderPane.setLeft(leftPane);
        helpScreenBoderPane.setCenter(scrollPane);
        homeButton.setOnAction(e->controller.handleHomeButtonRequest());
    }

    private void createLeftPane() throws IOException {
        leftPane = new VBox();
        homeButton = initializeButton(HOME_ICON.toString(), "Home", false);
        leftPane.setPadding(new Insets(80,0,0,50));
        leftPane.getChildren().addAll(homeButton);
    }

    @Override
    public BorderPane getBorderPane() {
        return helpScreenBoderPane;
    }

    public Button initializeButton(String icon, String name, boolean disabled) throws IOException {
        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");
        Button button = new Button(name);
        button.setDisable(disabled);
        button.setStyle("-fx-base: #C0C0C0;");
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            ImageView imageView = new ImageView(buttonImage);
            imageView.setFitHeight(18);
            imageView.setFitWidth(18);
            button.setGraphic(imageView);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return button;
    }
}
