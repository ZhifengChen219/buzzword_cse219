package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzwordController;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static settings.AppPropertyType.*;

/**
 * Created by Archi on 11/6/16.
 */
public class Workspace extends AppWorkspaceComponent{
    private final PropertyManager propertyManager = PropertyManager.getManager();
    protected AppTemplate app;
    protected AppGUI gui;
    protected BuzzwordController controller;
    private HomeScreen homeScreen;
    private LevelSelectionScreen levelSelectionScreen;
    private GamePlayScreen gamePlayScreen;
    private ProfileSettingScreen profileSettingScreen;
    private HelpScreen helpScreen;

    public Workspace(){}

    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (BuzzwordController) gui.getFileController();

        levelSelectionScreen = new LevelSelectionScreen(controller);

        homeScreen = new HomeScreen(controller);

        gamePlayScreen = new GamePlayScreen(controller);

        profileSettingScreen = new ProfileSettingScreen(controller);

        helpScreen = new HelpScreen(controller);
    }

    @Override
    public void initStyle() {

    }

    @Override
    public void reloadWorkspace() {

    }


    public  Screen getScreen(String name) {
        if (name.equals(propertyManager.getPropertyValue(HOMESCREEN)))
            return homeScreen;
        else if (name.equals(propertyManager.getPropertyValue(LEVELSELECTIONSCREEN)))
            return levelSelectionScreen;
        else if (name.equals(propertyManager.getPropertyValue(GAMEPLAYSCREEN)))
            return gamePlayScreen;
        else if(name.equals(propertyManager.getPropertyValue(PROFILESETTINGSCREEN)))
            return profileSettingScreen;
        else if(name.equals(propertyManager.getPropertyValue(HELPSCREEN)))
            return helpScreen;
        return null;
    }


}
