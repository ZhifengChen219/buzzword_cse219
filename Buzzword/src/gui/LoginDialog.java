package gui;

import apptemplate.AppTemplate;
import controller.BuzzwordController;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ui.AppGUI;

/**
 * Created by Archi on 11/8/16.
 */
public class LoginDialog extends Stage {
    private static LoginDialog singleton;
    private VBox outerVbox;
    private Scene scene;

    private HBox hbox1;
    private Label profilelNameLabel;
    private TextField profileNameTextField;

    private HBox hbox2;
    private Label passwordLabel;
    private PasswordField passwordTextField;

    private AppTemplate appTemplate;
    private AppGUI gui;
    private BuzzwordController controller;

    public static LoginDialog getSingleton() {
        if (singleton == null)
            singleton = new LoginDialog();
        return singleton;
    }

    public void init(Stage primaryStage) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        outerVbox = new VBox();
        hbox1 = new HBox();
        profilelNameLabel = new Label("Profile Name: ");
        profileNameTextField = new TextField();
        hbox1.getChildren().addAll(profilelNameLabel, profileNameTextField);
        hbox1.setSpacing(11);

        hbox2 = new HBox();
        passwordLabel = new Label("Password: ");
        passwordTextField = new PasswordField();
        hbox2.getChildren().addAll(passwordLabel, passwordTextField);
        hbox2.setSpacing(30);
        outerVbox.getChildren().addAll(hbox1, hbox2);
        outerVbox.setSpacing(8);
        outerVbox.setPadding(new Insets(10,10,10,10));
        outerVbox.setStyle("-fx-background-color: #E0E0E0;");
        scene = new Scene(outerVbox);
        this.setScene(scene);
        this.initStyle(StageStyle.TRANSPARENT);
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent ke) {
                if (ke.getCode() == KeyCode.ESCAPE) {
                    close();
                } else if(ke.getCode() == KeyCode.ENTER) {
                    gui = appTemplate.getGUI();
                    controller = (BuzzwordController) gui.getFileController();
                    controller.handleLoginRequest(profileNameTextField.getText(), passwordTextField.getText());
                }
            }
        });

    }

    public void setAppTemplate(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public String getProfileName() {
        return profileNameTextField.getText();
    }

    public String getPassword() {
        return passwordTextField.getText();
    }

    public void clearLoginPasswordField() {
        passwordTextField.clear();
    }

    public void clearProfileNameField() {
        profileNameTextField.clear();
    }


}
