package gui;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;

/**
 * Created by Archi on 11/6/16.
 */
public interface Screen {

    BorderPane getBorderPane();


}
