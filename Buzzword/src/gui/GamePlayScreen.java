package gui;

import apptemplate.AppTemplate;
import buzzword.Word;
import controller.BuzzwordController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.util.Duration;
import propertymanager.PropertyManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * Created by Archi on 11/6/16.
 */
public class GamePlayScreen  implements Screen{
    private final PropertyManager propertyManager = PropertyManager.getManager();
    private AppTemplate appTemplate;
    private BuzzwordController controller;

    private BorderPane gameplayScreenBoderpane;

    private VBox leftPane;
    private Button homeButton;
    private Button restartButton;
    private Button playNextButton;

    private Button[] circles;
    private Line[] lines;
    private VBox centerPane;
    private HBox hbox1;
    private Label modeNameLabel;
    private VBox letterGrid;
    private HBox hbox2;
    private HBox hbox3;
    private HBox hbox4;
    private HBox hbox5;
    private HBox hbox6;
    private HBox hbox7;
    private HBox hbox8;
    private HBox hbox9;
    private Label levelLabel;
    private HBox hbox10;
    private Button startButton;
    private Button pauseButton;

    private VBox rightPane;
    private Label timerLabel;
    private Label timerDigitLabel;
    private VBox timerVbox;
    private Timeline timeline;
    private Integer timeSecond;

    private VBox currentGuesseVbox;
    private HBox currentGuessHbox1;
    private Label currentGuessLabel;
    private HBox currentGuessHbox2;
    private Label currentGuesWordListLabel;
    private ArrayList<String> realWordList;

    private VBox guessedWordVbox;
    private TableView table;
    private TableColumn firstCol;
    private TableColumn secondCol;
    private final ObservableList<Word> guessedWordData =
            FXCollections.observableArrayList();

    private Label totalScoreLabel;
    private Label totalScoreDigitLabel;
    private HBox totalScoreHbox;
    private Label targetScoreLabel;
    private Label targetScoreDigitLabel;
    private HBox targetScoreHbox;

    private Node[] group;
    private Node[][] nodes;
    private ArrayList<String> wordList;
    private ArrayList<Integer> iss;
    private ArrayList<Integer> jss;
    private DropShadow shadow;

    private ArrayList<String> typedWordList;
    private ArrayList<Integer> iss2;
    private ArrayList<Integer> jss2;
    private boolean tracker = true;
    private boolean tracker2 = true;
    private boolean[][] track = new boolean[7][7];
    private boolean[][] track2 = new boolean[7][7];
    private boolean[][] trackforDisLetter = new boolean[7][7];
    private int firstRoundConter = 0;
    private int count = 0;
    private boolean[][] trackforDisPath = new boolean[7][7];
    private ArrayList<Boolean> trackHighlishtList = new ArrayList<Boolean>();

    public GamePlayScreen(BuzzwordController controller) throws IOException{
        this.controller = controller;
        gameplayScreenBoderpane = new BorderPane();
        createLeftPane();
        createCenterPane();
        createRightPane();
        gameplayScreenBoderpane.setLeft(leftPane);
        gameplayScreenBoderpane.setCenter(centerPane);
        gameplayScreenBoderpane.setRight(rightPane);
        leftPane.setPadding(new Insets(80,0,0,30));
        centerPane.setPadding(new Insets(60,0,0,20));
        centerPane.setSpacing(13);
        rightPane.setPadding(new Insets(60,15,0,0));

        startButton.setOnAction(e->controller.handleStartPlayingRequest());

        restartButton.setOnAction(e->{

            clearEveryNodes();
            restartWordType();
            clearHighlighting();
            currentGuesWordListLabel.setText(null);
            controller.setGameState(BuzzwordController.GameState.UNINITIALIZED);
            setTableBackgourndBack();
            controller.handleStartPlayingRequest();
        });
        pauseButton.setOnAction(e->controller.handlePauseButtonRequest());
        playNextButton.setOnAction(e->{

            setRestartButtonDisable(false);
            setStartButtonDisable(false);
            setPlayNextButtonDisable(true);
            pauseCountdown();
            disableNodes();
            setTimerDigitLabel(null);
            setStartPlayingButton();
            clearEveryNodes();
            restartWordType();
            clearHighlighting();
            setCurrentGuessLabel(null);
            setTableBackgourndBack();
            controller.setSelectGameModeLevel(controller.getSelectGameModeLevel()+1);
            setLevelLabel(String.valueOf(controller.getSelectGameModeLevel()+1));
        });

        wordList = new ArrayList<>();
        typedWordList = new ArrayList<>();
        realWordList = new ArrayList<>();
        iss = new ArrayList<>();
        jss = new ArrayList<>();
        iss2 = new ArrayList<>();
        jss2 = new ArrayList<>();
        shadow = new DropShadow();
        shadow.setColor(Color.RED);
        wordHighlighting();

    }

    public void setCurrentGuessLabel(String str) {
        currentGuesWordListLabel.setText(str);
    }

    private void wordHighlighting() {
        for(int i = 0; i < nodes.length; i++) {

            for (int j = 0; j < nodes[0].length; j++) {
                if(nodes[i][j] instanceof Button) {
                    Button b = (Button) nodes[i][j];

                    b.setOnDragDetected(event -> {
                        wordList.clear();
                        Dragboard db = b.startDragAndDrop(TransferMode.MOVE);
                        ClipboardContent content = new ClipboardContent();
                        content.putString(b.getText());
                        db.setContent(content);
                        Button b2 = new Button("");
                        b2.setStyle("-fx-background-color: transparent;");
                        db.setDragView(b2.snapshot(null, null));
                        event.consume();
                    });

                    b.setOnDragOver(event -> {
                        event.acceptTransferModes(TransferMode.MOVE);
                        event.consume();
                    });

                    b.setOnDragEntered(event -> {
                        if(b.equals(nodes[1][1])) {
                            return;
                        }

                        for(int i1 = 0; i1 < nodes.length; i1++) {
                            for(int j1 = 0; j1 < nodes[0].length; j1++) {
                                if(nodes[i1][j1] == null) {
                                    continue;
                                }
                                if(b.equals(nodes[i1][j1])) {
                                    if(track[i1][j1] == true) {
                                        return;
                                    }
                                    count = 0;
                                    for(int a = 0 ; a < trackforDisPath.length; a++) {
                                        for(int c = 0; c < trackforDisPath[0].length; c++) {
                                            if(nodes[a][c] instanceof Button) {
                                                if(trackforDisPath[a][c] == false) {
                                                    count++;
                                                }
                                            }

                                        }
                                    }
                                    if(count != 16) {

                                        if(i1-2<0) {
                                            if(j1-2<0) {
                                                if(trackforDisPath[i1][j1+2] == false &&
                                                        trackforDisPath[i1+2][j1] == false) {
                                                    return;
                                                }
                                            }else if(j1+2>6) {
                                                if(trackforDisPath[i1][j1-2] == false &&
                                                        trackforDisPath[i1+2][j1] == false) {
                                                    return;
                                                }
                                            }else {
                                                if(trackforDisPath[i1][j1-2] == false &&
                                                        trackforDisPath[i1][j1+2] == false &&
                                                        trackforDisPath[i1+2][j1] == false) {
                                                    return;
                                                }
                                            }
                                        } else if(i1+2>6) {
                                            if(j1-2<0) {
                                                if(trackforDisPath[i1][j1+2] == false &&
                                                        trackforDisPath[i1-2][j1] == false) {
                                                    return;
                                                }
                                            }else if(j1+2>6) {
                                                if(trackforDisPath[i1][j1-2] == false &&
                                                        trackforDisPath[i1-2][j1] == false) {
                                                    return;
                                                }
                                            }else {
                                                if(trackforDisPath[i1][j1-2] == false &&
                                                        trackforDisPath[i1][j1+2] == false &&
                                                        trackforDisPath[i1-2][j1] == false) {
                                                    return;
                                                }
                                            }
                                        }else if(j1-2<0) {
                                            if(trackforDisPath[i1-2][j1] == false &&
                                                    trackforDisPath[i1][j1+2] == false &&
                                                    trackforDisPath[i1+2][j1] == false) {
                                                return;
                                            }
                                        } else if(j1+2>6) {
                                            if(trackforDisPath[i1-2][j1] == false &&
                                                    trackforDisPath[i1][j1-2] == false &&
                                                    trackforDisPath[i1+2][j1] == false) {
                                                return;
                                            }
                                        } else {
                                            if(trackforDisPath[i1-2][j1] == false &&
                                                    trackforDisPath[i1][j1-2] == false &&
                                                    trackforDisPath[i1+2][j1] == false &&
                                                    trackforDisPath[i1][j1+2] == false) {
                                                return;
                                            }
                                        }
                                    }

                                    track[i1][j1] = true;
                                    trackforDisPath[i1][j1] = true;
                                    b.setEffect(shadow);
                                    iss.add(i1);
                                    jss.add(j1);
                                    lightupTheLines(iss,jss);
                                }
                            }
                        }
                        wordList.add(b.getText());
                        event.consume();
                    });

                    b.setOnDragDropped(event -> {
                        setCurrentGuesWordListLabel(wordList);
                    });

                    b.setOnDragDone(event -> {
                        Dragboard db = event.getDragboard();
                        iss.clear();
                        jss.clear();
                        for(int i12 = 0; i12 < nodes.length; i12++) {
                            for(int j12 = 0; j12 < nodes[0].length; j12++) {
                                if(nodes[i12][j12] == null) {
                                    continue;
                                }
                                nodes[i12][j12].setEffect(null);
                            }
                        }
                        for(int i3 = 0; i3 < track.length; i3++) {
                            for(int j3 = 0; j3 < track[0].length; j3++) {
                                track[i3][j3] = false;
                            }
                        }
                        for(int i4 = 0; i4 < trackforDisPath.length; i4++ ) {
                            for(int j4 = 0; j4 < trackforDisPath[0].length; j4++) {
                                trackforDisPath[i4][j4] = false;
                            }
                        }
                        count = 0;
                        event.consume();
                    });
                }
            }
        }
    }

    private void lightupTheLines(ArrayList iss, ArrayList jss) {
        if(iss.size() < 2 && jss.size() < 2) {
            return;
        }
        if((Integer)jss.get(0) < (Integer)jss.get(1) && iss.get(0) == iss.get(1)) {//horizontal
            Integer i = (Integer) iss.get(1);
            Integer j = (Integer) jss.get(1);
            nodes[i][j-1].setEffect(shadow);

        } else if((Integer)iss.get(0) < (Integer)iss.get(1) && jss.get(0) == jss.get(1) ) {//vertical
            Integer i = (Integer) iss.get(1);
            Integer j = (Integer) jss.get(1);
            nodes[i-1][j].setEffect(shadow);

        } else if((Integer)jss.get(0) > (Integer)jss.get(1) && iss.get(0) == iss.get(1)) {//horizontal-backward
            Integer i = (Integer)iss.get(0);
            Integer j = (Integer)jss.get(0);
            nodes[i][j-1].setEffect(shadow);

        } else if((Integer)iss.get(0) > (Integer)iss.get(1) && jss.get(0) == jss.get(1)) {//vertical-backward
            Integer i = (Integer)iss.get(0);
            Integer j = (Integer)jss.get(0);
            nodes[i-1][j].setEffect(shadow);

        }
        iss.remove(0);
        jss.remove(0);

    }

    private void createLeftPane() throws IOException {
        leftPane = new VBox();
        homeButton = initializeButton(HOME_ICON.toString(), "Home", false);
        restartButton = initializeButton(BIGPLAY_ICON.toString(), "Restart", true);
        playNextButton = initializeButton(BIGPLAY_ICON.toString(), "Play Next Level", true);
        homeButton.setMaxWidth(Double.MAX_VALUE);
        restartButton.setMaxWidth(Double.MAX_VALUE);
        playNextButton.setMaxWidth(Double.MAX_VALUE);
        leftPane.setSpacing(15);
        leftPane.getChildren().addAll(homeButton, restartButton, playNextButton);
        homeButton.setOnAction(e->controller.handleHomeButtonRequest());
    }

    public void setPlayNextButtonDisable(boolean b) {
        playNextButton.setDisable(b);
    }

    public void setRestartButtonDisable(boolean b) {
        restartButton.setDisable(b);
    }

    private void createRightPane() {
        rightPane = new VBox();

        timerVbox = new VBox();
        timerVbox.setStyle("-fx-background-color: #E0E0E0");
        timerLabel = new Label("Time Remaining");
        timerLabel.setStyle("-fx-font-size: 10pt;" +
                "-fx-text-fill: red;" +
                "-fx-underline: true;");
        timerDigitLabel = new Label();
        timerDigitLabel.setStyle("-fx-font-size: 10pt;" +
                "-fx-text-fill: red;" +
                "-fx-underline: true;");
        timerVbox.getChildren().addAll(timerLabel, timerDigitLabel);
        timerVbox.setAlignment(Pos.BASELINE_CENTER);
        timerVbox.setSpacing(5);
        timerVbox.setPadding(new Insets(10,10,10,10));



        currentGuesseVbox = new VBox();
        currentGuessHbox1 = new HBox();
        currentGuessLabel = new Label("CurrentGuess: ");
        currentGuessLabel.setStyle("-fx-font-size: 10pt;");
        currentGuessHbox1.getChildren().addAll(currentGuessLabel);
        currentGuessHbox2 = new HBox();
        currentGuesWordListLabel = new Label();
        currentGuessHbox2.getChildren().add(currentGuesWordListLabel);
        currentGuesseVbox.getChildren().addAll(currentGuessHbox1, currentGuessHbox2);
        currentGuesseVbox.setStyle("-fx-background-color: #E0E0E0");
        currentGuesseVbox.setPadding(new Insets(10,10,10,10));
        currentGuesseVbox.setSpacing(5);

        guessedWordVbox = new VBox();
        table = new TableView();
        firstCol = new TableColumn("Words");
        firstCol.setCellValueFactory(
                new PropertyValueFactory<Word, String>("word"));
        secondCol = new TableColumn("Scores");
        secondCol.setCellValueFactory(
                new PropertyValueFactory<Word, Integer>("score"));
        firstCol.setStyle("-fx-background-color: #E0E0E0");
        secondCol.setStyle("-fx-background-color: #E0E0E0");
        table.getColumns().addAll(firstCol, secondCol);
        table.setStyle("-fx-background-color: #E0E0E0");
        table.setPrefSize(150, 150);
        table.setItems(guessedWordData);
        guessedWordVbox.getChildren().addAll(table);


        totalScoreHbox = new HBox();
        totalScoreLabel = new Label("Total: ");
        totalScoreDigitLabel = new Label();
        totalScoreHbox.getChildren().addAll(totalScoreLabel, totalScoreDigitLabel);
        totalScoreLabel.setStyle("-fx-font-size: 10pt;");
        totalScoreHbox.setStyle("-fx-background-color: #E0E0E0");
        totalScoreHbox.setPadding(new Insets(10,10,10,10));
        totalScoreHbox.setAlignment(Pos.BASELINE_CENTER);

        targetScoreHbox = new HBox();
        targetScoreLabel = new Label("Target: ");
        targetScoreDigitLabel = new Label();
        targetScoreHbox.getChildren().addAll(targetScoreLabel, targetScoreDigitLabel);
        targetScoreLabel.setStyle("-fx-font-size: 10pt;");
        targetScoreHbox.setStyle("-fx-background-color: #E0E0E0");
        targetScoreHbox.setPadding(new Insets(10,10,10,10));
        targetScoreHbox.setAlignment(Pos.BASELINE_CENTER);

        rightPane.getChildren().addAll(timerVbox, currentGuesseVbox,guessedWordVbox,totalScoreHbox, targetScoreHbox);
        rightPane.setSpacing(10);

    }

    public void setStartButtonDisable(boolean b) {
        startButton.setDisable(b);
    }

    private void setCurrentGuesWordListLabel(List wordList) {
        realWordList.clear();
        realWordList.addAll(wordList);
        currentGuesWordListLabel.setText(realWordList.toString().replaceAll("[^A-Za-z]+", ""));

    }

    public void addGuessedWordData(Word word) {
        this.guessedWordData.add(word);
    }

    public void setTabelBackground(){
        firstCol.setStyle("-fx-background-color: #FF9999");
        secondCol.setStyle("-fx-background-color: #FF9999");
    }

    public void setTableBackgourndBack() {
        firstCol.setStyle("-fx-background-color: #E0E0E0");
        secondCol.setStyle("-fx-background-color: #E0E0E0");
    }

    public List getRealWordList() {
        return this.realWordList;
    }

    public void startCountdown(AppTemplate apptemplate) {

        this.appTemplate = apptemplate;
        if (timeline != null) {
            timeline.stop();
        }
        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(1),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                timeSecond--;
                                // update timerLabel
                                timerDigitLabel.setText(
                                        timeSecond.toString());
                                if (timeSecond <= 0) {
                                    timeline.stop();
                                    controller.displaySolution();
                                    disableNodes();
                                    appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
                                    setStartPlayingButton();
                                    setStartButtonDisable(true);
                                    setTabelBackground();
                                    controller.setGameState(BuzzwordController.GameState.UNINITIALIZED);
                                }
                            }
                        }
                ));
        timeline.playFromStart();
    }

    public void clearGuessedWordData() {
        guessedWordData.clear();
    }

    public void pauseCountdown() {
        if(timeline != null) {
            timeline.stop();
        }
    }

    public void resetTimeSecond(int i) {
        this.timeSecond = i;
        timerDigitLabel.setText(
                timeSecond.toString());
    }

    public void setTimerDigitLabel(String str) {
        timerDigitLabel.setText(str);
    }

    public void disableNodes() {
        for(int i = 0; i < nodes.length; i++) {
            for(int j = 0; j < nodes[0].length; j++) {
                if(nodes[i][j] == null){
                    continue;
                }
                nodes[i][j].setDisable(true);
            }
        }
    }

    private void createCenterPane() throws IOException{
        group = new Node[40];
        nodes = new Node[7][7];
        circles = new Button[16];
        lines = new Line[24];
        centerPane = new VBox();
        hbox1 = new HBox();
        modeNameLabel = new Label();
        hbox1.getChildren().addAll(modeNameLabel);
        hbox1.setAlignment(Pos.CENTER);

        letterGrid = new VBox();
        hbox2 = new HBox();
        double r = 22;
        circles[0] = new Button();
        circles[1] = new Button();
        circles[2] = new Button();
        circles[3] = new Button();
        lines[0] = new Line(0, 10, 40, 10);
        lines[1] = new Line(0, 10, 40, 10);
        lines[2] = new Line(0, 10, 40, 10);
        nodes[0][0] = circles[0];
        nodes[0][1] = lines[0];
        nodes[0][2] = circles[1];
        nodes[0][3] = lines[1];
        nodes[0][4] = circles[2];
        nodes[0][5] = lines[2];
        nodes[0][6] = circles[3];
        for(int i = 0; i < 4; i++){
            circles[i].setShape(new Circle(r));
            circles[i].setMinSize(2*r, 2*r);
            circles[i].setMaxSize(2*r, 2*r);
            circles[i].setStyle("-fx-base: #606060;");
            circles[i].setDisable(true);
        }
        hbox2.getChildren().addAll(circles[0], lines[0], circles[1], lines[1], circles[2], lines[2], circles[3]);
        hbox2.setAlignment(Pos.CENTER);
        hbox3 = new HBox();
        lines[3] = new Line(0,0,0,20);
        lines[4] = new Line(0,0,0,20);
        lines[5] = new Line(0,0,0,20);
        lines[6] = new Line(0,0,0,20);
        nodes[1][0] = lines[3];
        nodes[1][2] = lines[4];
        nodes[1][4] = lines[5];
        nodes[1][6] = lines[6];
        hbox3.getChildren().addAll(lines[3], lines[4], lines[5], lines[6]);
        hbox3.setSpacing(85);
        hbox3.setAlignment(Pos.CENTER);


        hbox4 = new HBox();
        circles[4] = new Button();
        circles[5] = new Button();
        circles[6] = new Button();
        circles[7] = new Button();
        lines[7] = new Line(0, 10, 40, 10);
        lines[8] = new Line(0, 10, 40, 10);
        lines[9] = new Line(0, 10, 40, 10);
        nodes[2][0] = circles[4];
        nodes[2][1] = lines[7];
        nodes[2][2] = circles[5];
        nodes[2][3] = lines[8];
        nodes[2][4] = circles[6];
        nodes[2][5] = lines[9];
        nodes[2][6] = circles[7];
        for(int i = 4; i < 8; i++){
            circles[i].setShape(new Circle(r));
            circles[i].setMinSize(2*r, 2*r);
            circles[i].setMaxSize(2*r, 2*r);
            circles[i].setStyle("-fx-base: #606060;");
            circles[i].setDisable(true);
        }
        hbox4.getChildren().addAll(circles[4], lines[7], circles[5], lines[8], circles[6], lines[9], circles[7]);
        hbox4.setAlignment(Pos.CENTER);
        hbox5 = new HBox();
        lines[10] = new Line(0,0,0,20);
        lines[11] = new Line(0,0,0,20);
        lines[12] = new Line(0,0,0,20);
        lines[13] = new Line(0,0,0,20);
        nodes[3][0] = lines[10];
        nodes[3][2] = lines[11];
        nodes[3][4] = lines[12];
        nodes[3][6] = lines[13];
        hbox5.getChildren().addAll(lines[10], lines[11], lines[12], lines[13]);
        hbox5.setSpacing(85);
        hbox5.setAlignment(Pos.CENTER);


        hbox6 = new HBox();
        circles[8] = new Button();
        circles[9] = new Button();
        circles[10] = new Button();
        circles[11] = new Button();
        lines[14] = new Line(0, 10, 40, 10);
        lines[15] = new Line(0, 10, 40, 10);
        lines[16] = new Line(0, 10, 40, 10);
        nodes[4][0] = circles[8];
        nodes[4][1] = lines[14];
        nodes[4][2] = circles[9];
        nodes[4][3] = lines[15];
        nodes[4][4] = circles[10];
        nodes[4][5] = lines[16];
        nodes[4][6] = circles[11];
        for(int i = 8; i < 12; i++){
            circles[i].setShape(new Circle(r));
            circles[i].setMinSize(2*r, 2*r);
            circles[i].setMaxSize(2*r, 2*r);
            circles[i].setStyle("-fx-base: #606060;");
            circles[i].setDisable(true);
        }
        hbox6.getChildren().addAll(circles[8], lines[14], circles[9], lines[15], circles[10], lines[16], circles[11]);
        hbox6.setAlignment(Pos.CENTER);
        hbox7 = new HBox();
        lines[17] = new Line(0,0,0,20);
        lines[18] = new Line(0,0,0,20);
        lines[19] = new Line(0,0,0,20);
        lines[20] = new Line(0,0,0,20);
        nodes[5][0] = lines[17];
        nodes[5][2] = lines[18];
        nodes[5][4] = lines[19];
        nodes[5][6] = lines[20];
        hbox7.getChildren().addAll(lines[17], lines[18], lines[19], lines[20]);
        hbox7.setSpacing(85);
        hbox7.setAlignment(Pos.CENTER);


        hbox8 = new HBox();
        circles[12] = new Button();
        circles[13] = new Button();
        circles[14] = new Button();
        circles[15] = new Button();
        lines[21] = new Line(0, 10, 40, 10);
        lines[22] = new Line(0, 10, 40, 10);
        lines[23] = new Line(0, 10, 40, 10);
        nodes[6][0] = circles[12];
        nodes[6][1] = lines[21];
        nodes[6][2] = circles[13];
        nodes[6][3] = lines[22];
        nodes[6][4] = circles[14];
        nodes[6][5] = lines[23];
        nodes[6][6] = circles[15];

        for(int i = 12; i < 16; i++){
            circles[i].setShape(new Circle(r));
            circles[i].setMinSize(2*r, 2*r);
            circles[i].setMaxSize(2*r, 2*r);
            circles[i].setStyle("-fx-base: #606060;");
            circles[i].setDisable(true);
        }
        hbox8.getChildren().addAll(circles[12], lines[21], circles[13], lines[22], circles[14], lines[23], circles[15]);
        hbox8.setAlignment(Pos.CENTER);
        letterGrid.getChildren().addAll(hbox2, hbox3, hbox4, hbox5, hbox6, hbox7, hbox8);

        for(Line line: lines) {
            line.setStrokeWidth(4);
        }

        hbox9 = new HBox();
        levelLabel = new Label();
        hbox9.getChildren().addAll(levelLabel);
        hbox9.setAlignment(Pos.CENTER);

        hbox10 = new HBox();
        startButton = initializeButton(BIGPLAY_ICON.toString(), null, false);
        pauseButton = initializeButton(PAUSE_ICON.toString(), null,false);
        hbox10.getChildren().addAll(startButton);
        hbox10.setAlignment(Pos.CENTER);
        centerPane.getChildren().addAll(hbox1,letterGrid, hbox9, hbox10);


    }

    public void setPasueButton() {
        hbox10.getChildren().remove(0);
        hbox10.getChildren().addAll(pauseButton);
    }

    public void setStartPlayingButton() {
        hbox10.getChildren().remove(0);
        hbox10.getChildren().addAll(startButton);
    }

    public void setModeNameLabel(String value) {
        modeNameLabel.setText(value);
        modeNameLabel.setStyle("-fx-font-size: 12pt;" +
                "-fx-text-fill: white;" +
                "-fx-underline: true;");
    }

    public void setLevelLabel(String value){
        levelLabel.setText("Level: "+value);
        levelLabel.setStyle("-fx-font-size: 12pt;" +
                "-fx-text-fill: white;" +
                "-fx-underline: true;");
    }

    private Integer score = 0;
    public void setTotalScoreDigitLabel(Integer i) {
        score = score+i;
        totalScoreDigitLabel.setText(String.valueOf(score));
    }

    public void setTargetScoreLabel(String str) {
        targetScoreDigitLabel.setText(str);
    }

    public int getTotalScore() {
        return Integer.parseInt(totalScoreDigitLabel.getText());
    }

    public int getTargetScore() {
        return Integer.parseInt(targetScoreDigitLabel.getText());
    }


    @Override
    public BorderPane getBorderPane() {
        return gameplayScreenBoderpane;
    }

    public Button initializeButton(String icon, String name, boolean disabled) throws IOException {
        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");
        Button button = new Button(name);
        button.setDisable(disabled);
        button.setStyle("-fx-base: #C0C0C0;");
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            ImageView imageView = new ImageView(buttonImage);
            imageView.setFitHeight(18);
            imageView.setFitWidth(18);
            button.setGraphic(imageView);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return button;
    }

    public Button[] getCirclesArray() {
        return circles;
    }

    public void hideLetterGrid() {
        for(Button button:circles) {
            button.setVisible(false);
        }
        for(Line line:lines) {
            line.setVisible(false);
        }
    }

    public void resumeLetterGrid() {
        for(Button button:circles) {
            button.setVisible(true);
        }
        for(Line line:lines) {
            line.setVisible(true);
        }
    }

    public void setButtonsDisable(boolean b) {
        for(Button button: circles) {
            button.setDisable(b);
        }
    }

    public void keyboardTypeLightup(char guess) {
        char correctguess = ' ';
        ArrayList<String> temp = new ArrayList<>();
        for(int i = 0; i < nodes.length; i++) {
            for(int j = 0; j < nodes[0].length; j++) {
                if(nodes[i][j] instanceof Button) {
                    Button b = (Button) nodes[i][j];
                    if(guess == b.getText().charAt(0)) {
                        correctguess = guess;
                        if(!temp.contains(String.valueOf(correctguess))) {
                            temp.add(String.valueOf(correctguess));
                        }

                        if(tracker2) {
                            firstRoundConter++;
                            iss2.add(i);
                            jss2.add(j);
                        }
                    }

                }
            }
        }
        tracker2 = false;
        typedWordList.addAll(temp);
        setCurrentGuesWordListLabel(typedWordList);
        typeLightup(iss2, jss2, correctguess);

    }

    public void typeLightup(ArrayList<Integer> iss2, ArrayList<Integer> jss2, char guess) {
        if(tracker) {
            for(int i = 0; i < iss2.size(); i++) {
                Integer ii = iss2.get(i);
                Integer jj = jss2.get(i);
                nodes[ii][jj].setEffect(shadow);

                if(ii-1>0){
                    nodes[ii-1][jj].setEffect(shadow);//up
                }
                if(ii+1<=6){
                    nodes[ii+1][jj].setEffect(shadow);//down
                }
                if(jj+1<=6){
                    nodes[ii][jj+1].setEffect(shadow);//right
                }
                if(jj-1>0){
                    nodes[ii][jj-1].setEffect(shadow);//left
                }
            }
            tracker = false;
            return;
        }

        for(int i = 0 ; i < iss2.size(); i++) {
            Integer ii = iss2.get(i);
            Integer jj = jss2.get(i);

            if(ii-2>=0){
                Button b = (Button)nodes[ii-2][jj];//up
                if(String.valueOf(guess).equals(b.getText())){
                    track[ii-1][jj] = true;
                    track[ii][jj] = true;
                    if(ii+1<=6){
                        track2[ii+1][jj] = true;//down
                    }
                    if(jj+1<=6){
                        track2[ii][jj+1] = true;//right
                    }
                    if(jj-1>0){
                        track2[ii][jj-1] = true;//left
                    }

                    for(int j = 0; j < iss2.size(); j++) {
                        if(j+1 < iss2.size() && iss2.size() >= 2) {
                            Button b1 = (Button) nodes[iss2.get(j)][jss2.get(j)];
                            Button b2 = (Button) nodes[iss2.get(j+1)][jss2.get(j+1)];
                            if(b1.getText().equals(b2.getText())) {
                                continue;
                            }
                        }
                        if(j+1 >= iss2.size() && iss2.size() >= 2) {
                            Button b1 = (Button) nodes[iss2.get(j)][jss2.get(j)];
                            Button b2 = (Button) nodes[iss2.get(j-1)][jss2.get(j-1)];
                            if(b1.getText().equals(b2.getText())) {
                                continue;
                            }
                        }
                        if(ii == iss2.get(j) && jj == jss2.get(j)){
                            continue;
                        }else if(ii-2 == iss2.get(j) && jj == jss2.get(j)) {
                            continue;
                        } else if(ii+2 == iss2.get(j) && jj == jss2.get(j)) {
                            continue;
                        }else if(ii == jss2.get(j) && jj+2 == jss2.get(j)) {
                            continue;
                        }else if(ii == jss2.get(j) && jj+2 == jss2.get(j)) {
                            continue;
                        }
                        else {
                                nodes[iss2.get(j)][jss2.get(j)].setEffect(null);
                            if(iss2.get(j)-1>0){
                                    nodes[iss2.get(j)-1][jss2.get(j)].setEffect(null);//up
                            }
                            if(iss2.get(j)+1<=6){
                                    nodes[iss2.get(j)+1][jss2.get(j)].setEffect(null);//down
                            }
                            if(jss2.get(j)+1<=6){
                                    nodes[iss2.get(j)][jss2.get(j)+1].setEffect(null);//right
                            }
                            if(jss2.get(j)-1>0){
                                    nodes[iss2.get(j)][jss2.get(j)-1].setEffect(null);//left
                            }
                        }
                    }


                    nodes[ii-2][jj].setEffect(shadow);
                    if(ii-3>0){
                        if(track2[ii-3][jj] == false)
                        nodes[ii-3][jj].setEffect(shadow);//up
                    }
                    if(ii-1<=6){
                        if(track2[ii-1][jj] == false)
                        nodes[ii-1][jj].setEffect(shadow);//down
                    }
                    if(jj+1<=6){
                        if(track2[ii-2][jj+1] == false)
                        nodes[ii-2][jj+1].setEffect(shadow);//right
                    }
                    if(jj-1>0){
                        if(track2[ii-2][jj-1] == false)
                        nodes[ii-2][jj-1].setEffect(shadow);//left
                    }

                }
            }
            if(ii+2<=6){
                Button b = (Button)nodes[ii+2][jj];//down
                if(String.valueOf(guess).equals(b.getText())){
                    track[ii+1][jj] = true;
                    track[ii][jj] = true;
                    if(ii-1>0){
                        track2[ii-1][jj] = true;//up
                    }
                    if(jj+1<=6){
                        track2[ii][jj+1] = true;//right
                    }
                    if(jj-1>0){
                        track2[ii][jj-1] = true;//left
                    }

                    for(int j = 0; j < iss2.size(); j++) {
                        if(j+1 < iss2.size() && iss2.size() >= 2) {
                            Button b1 = (Button) nodes[iss2.get(j)][jss2.get(j)];
                            Button b2 = (Button) nodes[iss2.get(j+1)][jss2.get(j+1)];
                            if(b1.getText().equals(b2.getText())) {
                                continue;
                            }
                        }
                        if(j+1 >= iss2.size() && iss2.size() >= 2) {
                            Button b1 = (Button) nodes[iss2.get(j)][jss2.get(j)];
                            Button b2 = (Button) nodes[iss2.get(j-1)][jss2.get(j-1)];
                            if(b1.getText().equals(b2.getText())) {
                                continue;
                            }
                        }
                        if(ii == iss2.get(j) && jj == jss2.get(j)){
                            continue;
                        }else if(ii-2 == iss2.get(j) && jj == jss2.get(j)) {
                            continue;
                        } else if(ii+2 == iss2.get(j) && jj == jss2.get(j)) {
                            continue;
                        }else if(ii == jss2.get(j) && jj+2 == jss2.get(j)) {
                            continue;
                        }else if(ii == jss2.get(j) && jj+2 == jss2.get(j)) {
                            continue;
                        } else {
                            nodes[iss2.get(j)][jss2.get(j)].setEffect(null);
                            if(iss2.get(j)-1>0){
                                nodes[iss2.get(j)-1][jss2.get(j)].setEffect(null);//up
                            }
                            if(iss2.get(j)+1<=6){
                                nodes[iss2.get(j)+1][jss2.get(j)].setEffect(null);//down
                            }
                            if(jss2.get(j)+1<=6){
                                nodes[iss2.get(j)][jss2.get(j)+1].setEffect(null);//right
                            }
                            if(jss2.get(j)-1>0){
                                nodes[iss2.get(j)][jss2.get(j)-1].setEffect(null);//left
                            }
                        }
                    }



                    nodes[ii+2][jj].setEffect(shadow);
                    if(ii+1>0){
                        if(track2[ii+1][jj] == false)
                        nodes[ii+1][jj].setEffect(shadow);//up
                    }
                    if(ii+3<=6){
                        if(track2[ii+3][jj] == false)
                        nodes[ii+3][jj].setEffect(shadow);//down
                    }
                    if(jj+1<=6){
                        if(track2[ii+2][jj+1] == false)
                        nodes[ii+2][jj+1].setEffect(shadow);//right
                    }
                    if(jj-1>0){
                        if(track2[ii+2][jj-1] == false)
                        nodes[ii+2][jj-1].setEffect(shadow);//left
                    }

                }
            }
            if(jj+2<=6){
                Button b = (Button)nodes[ii][jj+2];//right
                if(String.valueOf(guess).equals(b.getText())){
                    track[ii][jj+1] = true;
                    track[ii][jj] = true;
                    if(ii-1>0){
                        track2[ii-1][jj] = true;//up
                    }
                    if(ii+1<=6){
                        track2[ii+1][jj] = true;//down
                    }
                    if(jj-1>0){
                        track2[ii][jj-1] = true;//left
                    }

                    for(int j = 0; j < iss2.size(); j++) {
                        if(j+1 < iss2.size() && iss2.size() >= 2) {
                            Button b1 = (Button) nodes[iss2.get(j)][jss2.get(j)];
                            Button b2 = (Button) nodes[iss2.get(j+1)][jss2.get(j+1)];
                            if(b1.getText().equals(b2.getText())) {
                                continue;
                            }
                        }
                        if(j+1 >= iss2.size() && iss2.size() >= 2) {
                            Button b1 = (Button) nodes[iss2.get(j)][jss2.get(j)];
                            Button b2 = (Button) nodes[iss2.get(j-1)][jss2.get(j-1)];
                            if(b1.getText().equals(b2.getText())) {
                                continue;
                            }
                        }
                        if(ii == iss2.get(j) && jj == jss2.get(j)){
                            continue;
                        } else if(ii-2 == iss2.get(j) && jj == jss2.get(j)) {
                            continue;
                        } else if(ii+2 == iss2.get(j) && jj == jss2.get(j)) {
                            continue;
                        }else if(ii == jss2.get(j) && jj+2 == jss2.get(j)) {
                            continue;
                        }else if(ii == jss2.get(j) && jj+2 == jss2.get(j)) {
                            continue;
                        } else {
                                nodes[iss2.get(j)][jss2.get(j)].setEffect(null);
                            if(iss2.get(j)-1>0){
                                    nodes[iss2.get(j)-1][jss2.get(j)].setEffect(null);//up
                            }
                            if(iss2.get(j)+1<=6){
                                    nodes[iss2.get(j)+1][jss2.get(j)].setEffect(null);//down
                            }
                            if(jss2.get(j)+1<=6){
                                    nodes[iss2.get(j)][jss2.get(j)+1].setEffect(null);//right
                            }
                            if(jss2.get(j)-1>0){
                                    nodes[iss2.get(j)][jss2.get(j)-1].setEffect(null);//left
                            }
                        }
                    }

                    nodes[ii][jj+2].setEffect(shadow);
                    if(ii-1>=0){
                        if(track2[ii-1][jj+2] == false)
                        nodes[ii-1][jj+2].setEffect(shadow);//up
                    }
                    if(ii+1<=6){
                        if(track2[ii+1][jj+2] == false)
                        nodes[ii+1][jj+2].setEffect(shadow);//down
                    }
                    if(jj+3<=6){
                        if(track2[ii][jj+3] == false);
                        nodes[ii][jj+3].setEffect(shadow);//right
                    }
                    if(jj+1>=0){
                        if(track2[ii][jj+1] == false)
                        nodes[ii][jj+1].setEffect(shadow);//left
                    }

                }
            }
            if(jj-2>=0){
                Button b = (Button)nodes[ii][jj-2];//left
                if(String.valueOf(guess).equals(b.getText())){
                    track[ii][jj-1] = true;
                    track[ii][jj] = true;
                    if(ii-1>0){
                        track2[ii-1][jj] = true;//up
                    }
                    if(ii+1<=6){
                        track2[ii+1][jj] = true;//down
                    }
                    if(jj+1<=6){
                        track2[ii][jj+1] = true;//right
                    }

                    for(int j = 0; j < iss2.size(); j++) {

                        if(j+1 < iss2.size() && iss2.size() >= 2) {
                            Button b1 = (Button) nodes[iss2.get(j)][jss2.get(j)];
                            Button b2 = (Button) nodes[iss2.get(j+1)][jss2.get(j+1)];
                            if(b1.getText().equals(b2.getText())) {
                                continue;
                            }
                        }
                        if(j+1 >= iss2.size() && iss2.size() >= 2) {
                            Button b1 = (Button) nodes[iss2.get(j)][jss2.get(j)];
                            Button b2 = (Button) nodes[iss2.get(j-1)][jss2.get(j-1)];
                            if(b1.getText().equals(b2.getText())) {
                                continue;
                            }
                        }

                        if(ii == iss2.get(j) && jj == jss2.get(j)){
                            continue;
                        } else if(ii-2 == iss2.get(j) && jj == jss2.get(j)) {
                            continue;
                        } else if(ii+2 == iss2.get(j) && jj == jss2.get(j)) {
                            continue;
                        }else if(ii == jss2.get(j) && jj+2 == jss2.get(j)) {
                            continue;
                        }else if(ii == jss2.get(j) && jj+2 == jss2.get(j)) {
                            continue;
                        }
                        else {
                                nodes[iss2.get(j)][jss2.get(j)].setEffect(null);
                            if(iss2.get(j)-1>0){
                                    nodes[iss2.get(j)-1][jss2.get(j)].setEffect(null);//up
                            }
                            if(iss2.get(j)+1<=6){
                                    nodes[iss2.get(j)+1][jss2.get(j)].setEffect(null);//down
                            }
                            if(jss2.get(j)+1<=6){
                                    nodes[iss2.get(j)][jss2.get(j)+1].setEffect(null);//right
                            }
                            if(jss2.get(j)-1>0){
                                    nodes[iss2.get(j)][jss2.get(j)-1].setEffect(null);//left
                            }
                        }
                    }
                    nodes[ii][jj-2].setEffect(shadow);

                    if(ii-1>=0){
                        if(track2[ii-1][jj-2] == false)
                        nodes[ii-1][jj-2].setEffect(shadow);//up
                    }
                    if(ii+1<=6){
                        if(track2[ii+1][jj-2] == false)
                        nodes[ii+1][jj-2].setEffect(shadow);//down
                    }
                    if(jj-1<=6){
                        if(track2[ii][jj-1] == false)
                        nodes[ii][jj-1].setEffect(shadow);//right
                    }
                    if(jj-3>=0){
                        if(track2[ii][jj-3] == false)
                        nodes[ii][jj-3].setEffect(shadow);//left
                    }

                }
            }
            if(track[ii][jj] == false)
                nodes[ii][jj].setEffect(null);
            if(jj+1<=6){
                if(track[ii][jj+1] == false)
                    nodes[ii][jj+1].setEffect(null);//right
            }
            if(jj-1>=0){
                if(track[ii][jj-1] == false)
                    nodes[ii][jj-1].setEffect(null);//left
            }
            if(ii+1<=6){
                if(track[ii+1][jj] == false)
                    nodes[ii+1][jj].setEffect(null);//down
            }
            if(ii-1>=0) {
                if(track[ii-1][jj] == false)
                    nodes[ii-1][jj].setEffect(null);//up
            }
        }



        for(int j = 0; j < iss2.size(); j++) {
            Integer ii = iss2.get(j);
            Integer jj = jss2.get(j);
            if(ii-2<0) {
                if (jj - 2 < 0) {
                    if(track[ii][jj+2] == true) {
                        Button b1 = (Button)nodes[ii+2][jj];
                        if(String.valueOf(guess).equals(b1.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else if(track[ii+2][jj] == true) {
                        Button b1 = (Button)nodes[ii][jj+2];
                        if(String.valueOf(guess).equals(b1.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else {
                        Button b1 = (Button)nodes[ii+2][jj];
                        Button b2 = (Button)nodes[ii][jj+2];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }

                } else if (jj + 2 > 6) {
                    if(track[ii][jj-2] == true) {
                        Button b1 = (Button)nodes[ii+2][jj];
                        if(String.valueOf(guess).equals(b1.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else if(track[ii+2][jj] == true) {
                        Button b1 = (Button)nodes[ii][jj-2];
                        if(String.valueOf(guess).equals(b1.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else {
                        Button b1 = (Button)nodes[ii+2][jj];
                        Button b2 = (Button)nodes[ii][jj-2];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }

                } else {
                    if(track[ii][jj-2] == true) {
                        Button b1 = (Button)nodes[ii][jj+2];
                        Button b2 = (Button)nodes[ii+2][jj];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())){
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else if(track[ii][jj+2] == true) {
                        Button b1 = (Button)nodes[ii][jj-2];
                        Button b2 = (Button)nodes[ii+2][jj];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())){
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else if(track[ii+2][jj] == true) {
                        Button b1 = (Button)nodes[ii][jj-2];
                        Button b2 = (Button)nodes[ii][jj+2];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())){
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else {
                        Button b1 = (Button)nodes[ii][jj-2];
                        Button b2 = (Button)nodes[ii][jj+2];
                        Button b3 = (Button)nodes[ii+2][jj];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())
                                || String.valueOf(guess).equals(b3.getText())){
                            trackHighlishtList.add(true);
                            break;
                        }
                    }
                }
            }else if(ii+2>6) {
                if (jj - 2 < 0) {
                    if(track[ii][jj+2] == true) {
                        Button b1 = (Button)nodes[ii-2][jj];
                        if(String.valueOf(guess).equals(b1.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else if(track[ii-2][jj] == true) {
                        Button b1 = (Button)nodes[ii][jj+2];
                        if(String.valueOf(guess).equals(b1.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else {
                        Button b1 = (Button)nodes[ii-2][jj];
                        Button b2 = (Button)nodes[ii][jj+2];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }

                } else if (jj + 2 > 6) {
                    if(track[ii][jj-2] == true) {
                        Button b1 = (Button)nodes[ii-2][jj];
                        if(String.valueOf(guess).equals(b1.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else if(track[ii-2][jj] == true) {
                        Button b1 = (Button)nodes[ii][jj-2];
                        if(String.valueOf(guess).equals(b1.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else {
                        Button b1 = (Button)nodes[ii-2][jj];
                        Button b2 = (Button)nodes[ii][jj-2];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())) {
                            trackHighlishtList.add(true);
                            break;
                        }
                    }

                } else {
                    if(track[ii][jj-2] == true) {
                        Button b1 = (Button)nodes[ii][jj+2];
                        Button b2 = (Button)nodes[ii-2][jj];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())){
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else if(track[ii][jj+2] == true) {
                        Button b1 = (Button)nodes[ii][jj-2];
                        Button b2 = (Button)nodes[ii-2][jj];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())){
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else if(track[ii-2][jj] == true) {
                        Button b1 = (Button)nodes[ii][jj-2];
                        Button b2 = (Button)nodes[ii][jj+2];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())){
                            trackHighlishtList.add(true);
                            break;
                        }
                    }else {
                        Button b1 = (Button)nodes[ii][jj-2];
                        Button b2 = (Button)nodes[ii][jj+2];
                        Button b3 = (Button)nodes[ii-2][jj];
                        if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())
                                || String.valueOf(guess).equals(b3.getText())){
                            trackHighlishtList.add(true);
                            break;
                        }
                    }
                }
            }else if(jj-2<0) {
                if(track[ii+2][jj] == true) {
                    Button b1 = (Button)nodes[ii][jj+2];
                    Button b2 = (Button)nodes[ii-2][jj];
                    if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())){
                        trackHighlishtList.add(true);
                        break;
                    }
                }else if(track[ii-2][jj] == true) {
                    Button b1 = (Button)nodes[ii][jj+2];
                    Button b2 = (Button)nodes[ii+2][jj];
                    if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())){
                        trackHighlishtList.add(true);
                        break;
                    }
                }else {
                    Button b1 = (Button)nodes[ii+2][jj];
                    Button b2 = (Button)nodes[ii][jj+2];
                    Button b3 = (Button)nodes[ii-2][jj];
                    if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())
                            || String.valueOf(guess).equals(b3.getText())){
                        trackHighlishtList.add(true);
                        break;
                    }
                }
            }else if(jj+2>6){
                if(track[ii+2][jj] == true) {
                    Button b1 = (Button)nodes[ii][jj-2];
                    Button b2 = (Button)nodes[ii-2][jj];
                    if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())){
                        trackHighlishtList.add(true);
                        break;
                    }
                }else if(track[ii-2][jj] == true) {
                    Button b1 = (Button)nodes[ii][jj-2];
                    Button b2 = (Button)nodes[ii+2][jj];
                    if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())){
                        trackHighlishtList.add(true);
                        break;
                    }
                }else {
                    Button b1 = (Button)nodes[ii+2][jj];
                    Button b2 = (Button)nodes[ii][jj-2];
                    Button b3 = (Button)nodes[ii-2][jj];
                    if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())
                            || String.valueOf(guess).equals(b3.getText())){
                        trackHighlishtList.add(true);
                        break;
                    }
                }
            }else {
                if(track[ii-2][jj] == true) {
                    Button b1 = (Button)nodes[ii+2][jj];
                    Button b2 = (Button)nodes[ii][jj-2];
                    Button b3 = (Button)nodes[ii][jj+2];
                    if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())
                            || String.valueOf(guess).equals(b3.getText())){
                        trackHighlishtList.add(true);
                        break;
                    }
                }else if(track[ii+2][jj] == true) {
                    Button b1 = (Button)nodes[ii-2][jj];
                    Button b2 = (Button)nodes[ii][jj-2];
                    Button b3 = (Button)nodes[ii][jj+2];
                    if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())
                            || String.valueOf(guess).equals(b3.getText())){
                        trackHighlishtList.add(true);
                        break;
                    }
                }else if(track[ii][jj-2] == true) {
                    Button b1 = (Button)nodes[ii-2][jj];
                    Button b2 = (Button)nodes[ii+2][jj];
                    Button b3 = (Button)nodes[ii][jj+2];
                    if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())
                            || String.valueOf(guess).equals(b3.getText())){
                        trackHighlishtList.add(true);
                        break;
                    }
                }else if(track[ii][jj+2] == true) {
                    Button b1 = (Button)nodes[ii-2][jj];
                    Button b2 = (Button)nodes[ii+2][jj];
                    Button b3 = (Button)nodes[ii][jj-2];
                    if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())
                            || String.valueOf(guess).equals(b3.getText())){
                        trackHighlishtList.add(true);
                        break;
                    }
                }else {
                    Button b1 = (Button)nodes[ii-2][jj];
                    Button b2 = (Button)nodes[ii+2][jj];
                    Button b3 = (Button)nodes[ii][jj-2];
                    Button b4 = (Button)nodes[ii][jj+2];
                    if(String.valueOf(guess).equals(b1.getText()) || String.valueOf(guess).equals(b2.getText())
                            || String.valueOf(guess).equals(b3.getText())
                            || String.valueOf(guess).equals(b4.getText())){
                        trackHighlishtList.add(true);
                        break;
                    }
                }
            }
        }
        if(!trackHighlishtList.contains(true)) {
            restartWordType();
            clearHighlighting();
            trackHighlishtList.clear();
        }



        ArrayList<Integer> listI = new ArrayList();
        ArrayList<Integer> listJ = new ArrayList();

        if(iss2.size() == 1) {
            for(int k = 0 ; k < 1; k++) {
                Integer ii = iss2.get(k);
                Integer jj = jss2.get(k);

                if(ii-2>=0) {
                    Button b = (Button) nodes[ii - 2][jj];//up
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii-2);
                        listJ.add(jj);

                    }
                }
                if(ii+2<=6) {
                    Button b = (Button) nodes[ii + 2][jj];//down
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii+2);
                        listJ.add(jj);
                    }
                }
                if(jj+2<=6) {
                    Button b = (Button) nodes[ii][jj + 2];//right
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii);
                        listJ.add(jj+2);
                    }
                }
                if(jj-2>=0) {
                    Button b = (Button) nodes[ii][jj - 2];//left
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii);
                        listJ.add(jj-2);
                    }
                }
            }
        } else if(iss2.size() == 2) {
            for(int k = 0 ; k < 2; k++) {
                Integer ii = iss2.get(k);
                Integer jj = jss2.get(k);

                if(ii-2>=0) {
                    Button b = (Button) nodes[ii - 2][jj];//up
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii-2);
                        listJ.add(jj);

                    }
                }
                if(ii+2<=6) {
                    Button b = (Button) nodes[ii + 2][jj];//down
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii+2);
                        listJ.add(jj);
                    }
                }
                if(jj+2<=6) {
                    Button b = (Button) nodes[ii][jj + 2];//right
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii);
                        listJ.add(jj+2);
                    }
                }
                if(jj-2>=0) {
                    Button b = (Button) nodes[ii][jj - 2];//left
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii);
                        listJ.add(jj-2);
                    }
                }
            }
        }else if(iss2.size() == 3) {
            for(int k = 0 ; k < 3; k++) {
                Integer ii = iss2.get(k);
                Integer jj = jss2.get(k);

                if(ii-2>=0) {
                    Button b = (Button) nodes[ii - 2][jj];//up
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii-2);
                        listJ.add(jj);

                    }
                }
                if(ii+2<=6) {
                    Button b = (Button) nodes[ii + 2][jj];//down
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii+2);
                        listJ.add(jj);
                    }
                }
                if(jj+2<=6) {
                    Button b = (Button) nodes[ii][jj + 2];//right
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii);
                        listJ.add(jj+2);
                    }
                }
                if(jj-2>=0) {
                    Button b = (Button) nodes[ii][jj - 2];//left
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii);
                        listJ.add(jj-2);
                    }
                }
            }
        } else  {
            for(int k = 0 ; k < firstRoundConter; k++) {
                Integer ii = iss2.get(k);
                Integer jj = jss2.get(k);

                if(ii-2>=0) {
                    Button b = (Button) nodes[ii - 2][jj];//up
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii-2);
                        listJ.add(jj);

                    }
                }
                if(ii+2<=6) {
                    Button b = (Button) nodes[ii + 2][jj];//down
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii+2);
                        listJ.add(jj);
                    }
                }
                if(jj+2<=6) {
                    Button b = (Button) nodes[ii][jj + 2];//right
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii);
                        listJ.add(jj+2);
                    }
                }
                if(jj-2>=0) {
                    Button b = (Button) nodes[ii][jj - 2];//left
                    if (String.valueOf(guess).equals(b.getText())) {
                        listI.add(ii);
                        listJ.add(jj-2);
                    }
                }
            }
            firstRoundConter = 1;
        }
        iss2.clear();
        jss2.clear();
        iss2.addAll(listI);
        jss2.addAll(listJ);

    }

    public void displayWord(String word) {
        for(int i = 0; i < word.length(); i++) {
            keyboardTypeLightup(word.charAt(i));
        }
    }

    public void clearHighlighting() {
        for(int i12 = 0; i12 < nodes.length; i12++) {
            for(int j12 = 0; j12 < nodes[0].length; j12++) {
                if(nodes[i12][j12] == null) {
                    continue;
                }
                nodes[i12][j12].setEffect(null);
            }
        }
    }
    private void clearTrack() {
        for(int i = 0; i < track.length; i++) {
            for(int j = 0; j < track[0].length; j++) {
                track[i][j] =false;
            }
        }
    }
    private void clearTrack2() {
        for(int i = 0; i < track2.length; i++) {
            for(int j = 0; j < track2[0].length; j++) {
                track2[i][j] =false;
            }
        }
    }

    public void setCurrentGuesWordListLabel(String str) {
        this.currentGuesWordListLabel.setText(str);
    }

    public void restartWordType() {
        setCurrentGuesWordListLabel("");
        typedWordList.clear();
        clearTrack();
        clearTrack2();
        tracker = true;
        tracker2 = true;
        firstRoundConter = 0;
        iss2.clear();
        jss2.clear();
    }

    public void clearEveryNodes() {
        for(int i = 0 ; i < circles.length;i++) {
            circles[i].setText(null);
        }
        totalScoreDigitLabel.setText(null);
        totalScoreDigitLabel.setText(null);
        score = 0;
        guessedWordData.clear();
    }

}