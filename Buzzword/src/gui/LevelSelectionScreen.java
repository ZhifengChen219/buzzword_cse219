package gui;

import apptemplate.AppTemplate;
import controller.BuzzwordController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import propertymanager.PropertyManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static settings.AppPropertyType.HOME_ICON;
import static settings.AppPropertyType.PROFILE_ICON;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * Created by Archi on 11/6/16.
 */
public class LevelSelectionScreen implements Screen {
    private final PropertyManager propertyManager = PropertyManager.getManager();
    private BuzzwordController controller;

    private BorderPane levelSelectionBoderPane;

    private VBox leftPane;
    private Button viewProfileButton;
    private Button homeButton;

    private VBox centerPane;
    private HBox hbox1;
    private Label modeNameLabel;
    private String modeValue;
    private VBox levelGrid;
    private HBox hbox2;
    private HBox hbox3;
    private Button[] circles;

    private int avaliableLevel;


    public LevelSelectionScreen(BuzzwordController controller) throws IOException{
        this.controller = controller;
        levelSelectionBoderPane = new BorderPane();
        createLeftPane();
        createCenterPane();
        levelSelectionBoderPane.setLeft(leftPane);
        levelSelectionBoderPane.setCenter(centerPane);
        homeButton.setOnAction(e->controller.handleHomeButtonRequest());
    }

    private void createLeftPane() throws IOException{
        leftPane = new VBox();
        viewProfileButton = initializeButton(PROFILE_ICON.toString(), "View Profile", false);
        homeButton = initializeButton(HOME_ICON.toString(), "Home", false);
        viewProfileButton.setMaxWidth(Double.MAX_VALUE);
        homeButton.setMaxWidth(Double.MAX_VALUE);
        leftPane.setSpacing(15);
        leftPane.setPadding(new Insets(80,0,0,50));
        leftPane.getChildren().addAll(viewProfileButton, homeButton);
    }

    private void createCenterPane() {
        centerPane = new VBox();

        hbox1 = new HBox();
        modeNameLabel = new Label(null);
        hbox1.getChildren().addAll(modeNameLabel);
        hbox1.setAlignment(Pos.CENTER);

        circles = new Button[8];
        double r = 25;
        levelGrid = new VBox();
        hbox2 = new HBox();
        hbox3 = new HBox();

        Button cir1 = new Button("1");
        Button cir2 = new Button("2");
        Button cir3 = new Button("3");
        Button cir4 = new Button("4");
        Button cir5 = new Button("5");
        Button cir6 = new Button("6");
        Button cir7 = new Button("7");
        Button cir8 = new Button("8");

        circles[0] = cir1;
        circles[1] = cir2;
        circles[2] = cir3;
        circles[3] = cir4;
        circles[4] = cir5;
        circles[5] = cir6;
        circles[6] = cir7;
        circles[7] = cir8;

        cir1.setOnAction(e->controller.handleSelectModeRequest(0));
        cir2.setOnAction(e->controller.handleSelectModeRequest(1));
        cir3.setOnAction(e->controller.handleSelectModeRequest(2));
        cir4.setOnAction(e->controller.handleSelectModeRequest(3));
        cir5.setOnAction(e->controller.handleSelectModeRequest(4));
        cir6.setOnAction(e->controller.handleSelectModeRequest(5));
        cir7.setOnAction(e->controller.handleSelectModeRequest(6));
        cir8.setOnAction(e->controller.handleSelectModeRequest(7));

        for(int i = 0; i < 8; i++){
            circles[i].setShape(new Circle(r));
            circles[i].setMinSize(2*r, 2*r);
            circles[i].setMaxSize(2*r, 2*r);
            circles[i].setStyle("-fx-base: #606060;");
            circles[i].setDisable(true);
        }


        for(int i = 0; i < 4; i++) {
            hbox2.getChildren().add(circles[i]);
        }
        for(int i = 4; i < 8; i++) {
            hbox3.getChildren().add(circles[i]);
        }

        hbox2.setSpacing(35);
        hbox3.setSpacing(35);
        hbox2.setAlignment(Pos.CENTER);
        hbox3.setAlignment(Pos.CENTER);
        levelGrid.getChildren().addAll(hbox2, hbox3);
        levelGrid.setSpacing(10);


        centerPane.getChildren().addAll(hbox1,levelGrid);
        centerPane.setSpacing(10);
        centerPane.setPadding(new Insets(60, 115, 0, 0));
    }

    public void setModeValue(String value){
        this.modeValue = value;
        modeNameLabel.setText(modeValue);
        modeNameLabel.setStyle("-fx-font-size: 12pt;" +
                                "-fx-text-fill: white;" +
                                "-fx-underline: true;");
    }

    public void setAvaliableLevel(int avaliableLevel){
        for(int i = 0; i < circles.length; i++) {
            circles[i].setDisable(true);
        }
        this.avaliableLevel = avaliableLevel;
        for(int i = 0; i < avaliableLevel; i++) {
            circles[i].setDisable(false);
        }
    }



    @Override
    public BorderPane getBorderPane() {
        return levelSelectionBoderPane;
    }

    public Button initializeButton(String icon, String name, boolean disabled) throws IOException {
        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");
        Button button = new Button(name);
        button.setDisable(disabled);
        button.setStyle("-fx-base: #C0C0C0;");
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            ImageView imageView = new ImageView(buttonImage);
            imageView.setFitHeight(18);
            imageView.setFitWidth(18);
            button.setGraphic(imageView);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return button;
    }
}
