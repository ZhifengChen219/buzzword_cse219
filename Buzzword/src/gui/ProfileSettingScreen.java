package gui;

import apptemplate.AppTemplate;
import controller.BuzzwordController;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static settings.AppPropertyType.HOME_ICON;
import static settings.AppPropertyType.PROFILE_ICON;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * Created by Archi on 12/11/16.
 */
public class ProfileSettingScreen implements Screen {
    private final PropertyManager propertyManager = PropertyManager.getManager();
    private BuzzwordController controller;

    private BorderPane profileSettingBorderPane;

    private VBox leftPane;
    private Button homeButton;
    private Button changePasswordButton;

    private VBox centerPane;
    private HBox hbox;
    private Label nameLabel;
    private Label actualNameLabel;
    private HBox hbox2;
    private HBox hbox3;
    private Label enterPassword;
    private PasswordField passwordField;
    private Label repeatEnterPassword;
    private PasswordField passwordField2;
    private Button enter;


    public ProfileSettingScreen(BuzzwordController controller) throws IOException {
        AppMessageDialogSingleton appMessageDialogSingleton = AppMessageDialogSingleton.getSingleton();
        this.controller = controller;
        profileSettingBorderPane = new BorderPane();
        createLeftPane();
        createCenterPane();
        profileSettingBorderPane.setLeft(leftPane);
        profileSettingBorderPane.setCenter(centerPane);
        homeButton.setOnAction(e -> controller.handleHomeButtonRequest());
        changePasswordButton.setOnAction(e->setChangePasswordVisible(true));
        enter.setOnAction(e->{
           if(passwordField.getText().isEmpty()){
                appMessageDialogSingleton.show(null, "Your password field is empty");
            }else if(passwordField2.getText().isEmpty()){
                appMessageDialogSingleton.show(null, "Your repeat password field is empty");
            } else if(!passwordField.getText().equals(passwordField2.getText())){
                appMessageDialogSingleton.show(null, "Your passwords don't match.");
               passwordField.clear();
               passwordField2.clear();
            } else {
               controller.handleChangePassowrdRequest(passwordField.getText());
            }

        });
    }

    private void createLeftPane() throws IOException {
        leftPane = new VBox();
        changePasswordButton = initializeButton(PROFILE_ICON.toString(), "Change Password", false);
        homeButton = initializeButton(HOME_ICON.toString(), "Home", false);
        changePasswordButton.setMaxWidth(Double.MAX_VALUE);
        homeButton.setMaxWidth(Double.MAX_VALUE);
        leftPane.setSpacing(15);
        leftPane.setPadding(new Insets(80,0,0,50));
        leftPane.getChildren().addAll(homeButton, changePasswordButton);
    }

    private void createCenterPane() {
        centerPane = new VBox();
        hbox = new HBox();
        nameLabel = new Label("Name:  ");
        nameLabel.setStyle("-fx-font-size: 22pt;" +
                "-fx-text-fill: white;" +
                "-fx-underline: true;");
        actualNameLabel = new Label();
        actualNameLabel.setStyle("-fx-font-size: 22pt;" +
                "-fx-text-fill: white;" +
                "-fx-underline: true;");
        hbox = new HBox();
        hbox.getChildren().addAll(nameLabel, actualNameLabel);

        hbox2 = new HBox();
        enterPassword = new Label("Enter New Password:");
        enterPassword.setStyle("-fx-font-size: 13pt;" +
                        "-fx-text-fill: white;");
        passwordField = new PasswordField();
        hbox2.getChildren().addAll(enterPassword, passwordField);

        hbox3 = new HBox();
        repeatEnterPassword = new Label("Repeat New Password:");
        repeatEnterPassword.setStyle("-fx-font-size: 13pt;" +
                "-fx-text-fill: white;");
        passwordField2 = new PasswordField();
        enter = new Button("Enter");
        hbox3.getChildren().addAll(repeatEnterPassword, passwordField2, enter);
        hbox2.setSpacing(20);
        hbox3.setSpacing(5);
        setChangePasswordVisible(false);

        centerPane.getChildren().addAll(hbox, hbox2, hbox3);
        centerPane.setPadding(new Insets(50, 0, 0, 60));
        centerPane.setSpacing(8);
    }

    public void setActualNameLabel(String name) {
        actualNameLabel.setText(name);
    }

    @Override
    public BorderPane getBorderPane() {
        return profileSettingBorderPane;
    }

    public void setChangePasswordVisible(boolean b) {
        hbox2.setVisible(b);
        hbox3.setVisible(b);
    }

    public void clearPasswordField() {
        passwordField.clear();
        passwordField2.clear();
    }

    public Button initializeButton(String icon, String name, boolean disabled) throws IOException {
        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");
        Button button = new Button(name);
        button.setDisable(disabled);
        button.setStyle("-fx-base: #C0C0C0;");
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            ImageView imageView = new ImageView(buttonImage);
            imageView.setFitHeight(18);
            imageView.setFitWidth(18);
            button.setGraphic(imageView);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return button;
    }
}
