package controller;

import apptemplate.AppTemplate;
import buzzword.Boggle;
import buzzword.BoggleBoard;
import buzzword.Word;
import data.GameData;
import data.GameDataHug;
import gui.*;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import propertymanager.PropertyManager;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static settings.AppPropertyType.*;

/**
 * Created by Archi on 11/6/16.
 */
public class BuzzwordController implements FileController {
    public enum GameState {
        UNINITIALIZED,
        STARTED_PLAYING
    }
    private GameState gamestate;
    final KeyCombination keyComb1 = new KeyCodeCombination(KeyCode.V,
            KeyCombination.CONTROL_DOWN);
    final KeyCombination keyComb2 = new KeyCodeCombination(KeyCode.S,
            KeyCombination.CONTROL_DOWN);
    private final PropertyManager propertyManager = PropertyManager.getManager();
    private  AppTemplate appTemplate;
    private LoginDialog loginDialog;
    private CreateNewProfileDialog createNewProfileDialog;
    private int selectGameModeLevel;
    private String selectGameModeValue;
    private GameDataHug gameDataHug;
    private GameData gameData;
    private Boggle.Trie animals;
    private Boggle.Trie sports;
    private Boggle.Trie restaurant;
    private HashSet<String> wordAnswerset = null;
    private BoggleBoard boggleBoard = null;
    private Boggle boggle = null;

    public BuzzwordController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;

    }
    @Override
    public void handleCloseRequest() {
        if(gamestate.equals(GameState.STARTED_PLAYING)) {
            PropertyManager            propertyManager   = PropertyManager.getManager();
            YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
            yesNoCancelDialog.show("", "You are still playing, do you want to leave?");
            if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)){
                System.exit(0);
                gameData = null;
            }
        }
        if(gamestate.equals(GameState.UNINITIALIZED)) {
            System.exit(0);
            gameData = null;
        }

    }

    @Override
    public void handleCreateNewProfileRequest(String profileName, String password) {
        AppMessageDialogSingleton appMessageDialogSingleton = AppMessageDialogSingleton.getSingleton();
        try {
            HashMap hashMap = (HashMap)load();
            if(hashMap == null) {
                GameData gameData = new GameData(profileName, password);
                GameDataHug gameDataHug = (GameDataHug) appTemplate.getDataComponent();
                gameDataHug.addGameData(profileName, gameData);
                save();
                appMessageDialogSingleton.show(null, "Your profile has successfully created.");
                createNewProfileDialog.close();
            } else if(hashMap.containsKey(profileName)) {
                appMessageDialogSingleton.show(null, "Your profile name has been used by another user");
                createNewProfileDialog.clearProfileNameTextField();
                createNewProfileDialog.clearPasswordTextField();
                createNewProfileDialog.clearConfirmPasswordTextField();
            } else {
                GameData gameData = new GameData(profileName, password);
                GameDataHug gameDataHug = (GameDataHug) appTemplate.getDataComponent();
                gameDataHug.setGamedataHugMap(hashMap);
                gameDataHug.addGameData(profileName, gameData);
                save();
                appMessageDialogSingleton.show(null, "Your profile has successfully created.");
                createNewProfileDialog.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void handleChangePassowrdRequest(String password) {
        AppMessageDialogSingleton appMessageDialogSingleton = AppMessageDialogSingleton.getSingleton();
        gameData.setPassword(password);
        realSave();
        appMessageDialogSingleton.show(null, "Your password has successfully upgraded.");
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        ProfileSettingScreen profileSettingScreen = (ProfileSettingScreen) workspace.getScreen(propertyManager.getPropertyValue(PROFILESETTINGSCREEN));
        profileSettingScreen.clearPasswordField();
        profileSettingScreen.setChangePasswordVisible(false);
    }

    public void createNewProfileDialog() {
        createNewProfileDialog = CreateNewProfileDialog.getSingleton();
        createNewProfileDialog.setAppTemplate(appTemplate);
        createNewProfileDialog.clearProfileNameTextField();
        createNewProfileDialog.clearPasswordTextField();
        createNewProfileDialog.clearConfirmPasswordTextField();
        createNewProfileDialog.showAndWait();
    }

    @Override
    public void handleLoginRequest(String username, String password) {
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        HomeScreen homeScreen = (HomeScreen) workspace.getScreen(propertyManager.getPropertyValue(HOMESCREEN));
        AppMessageDialogSingleton appMessageDialogSingleton = AppMessageDialogSingleton.getSingleton();

        try {
            HashMap hashMap = (HashMap)load();
            if(hashMap == null){
                appMessageDialogSingleton.show(null, "Sorry, we don't recognize that user");
                loginDialog.clearProfileNameField();
                loginDialog.clearLoginPasswordField();
                return;
            }

            if(!hashMap.containsKey(username)) {
                appMessageDialogSingleton.show(null, "Sorry, we don't recognize that user");
                loginDialog.clearProfileNameField();
                loginDialog.clearLoginPasswordField();
            }else {
                GameData gameData = (GameData) hashMap.get(username);
                if(gameData.getPassword().equals(password)) {
                    appMessageDialogSingleton.show("Welcome", "Hello, " + username);
                    this.gameData = gameData;
                    loginDialog.close();
                    loginDialog.clearProfileNameField();
                    loginDialog.clearLoginPasswordField();
                    if(homeScreen != null){
                        homeScreen.setLeftVBox2();
                        appTemplate.getGUI().getPrimaryScene().addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
                            @Override
                            public void handle(KeyEvent event) {
                                if (keyComb1.match(event)) {
                                    createProfileSettingScreen();
                                }else if (keyComb2.match(event)) {
                                    createGamePlayScreen();
                                }
                            }
                        });
                    }
                }else {
                    appMessageDialogSingleton.show("Wrong", "Your password is incorrect.");
                    loginDialog.clearLoginPasswordField();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void createLoginDialog() {
        loginDialog = LoginDialog.getSingleton();
        loginDialog.setAppTemplate(appTemplate);
        loginDialog.showAndWait();
    }

    @Override
    public void handleLogOutRequest() {
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        HomeScreen homeScreen = (HomeScreen) workspace.getScreen(propertyManager.getPropertyValue(HOMESCREEN));
        homeScreen.setLeftVBox1();
        this.gameData = null;
        setGameState(GameState.UNINITIALIZED);
    }

    @Override
    public void handleStartPlayingRequest() {
        AppMessageDialogSingleton appMessageDialogSingleton = AppMessageDialogSingleton.getSingleton();
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        GamePlayScreen gamePlayScreen = (GamePlayScreen) workspace.getScreen(
                propertyManager.getPropertyValue(GAMEPLAYSCREEN));
        gamePlayScreen.setButtonsDisable(false);
        gamePlayScreen.setPasueButton();
        gamePlayScreen.setRestartButtonDisable(false);

        if(gamestate.equals(GameState.UNINITIALIZED)) {

            if(selectGameModeValue.equals("Animals")){
                initAnimalsDictionary();
                boggle = new Boggle(animals);
            }else if(selectGameModeValue.equals("Sports")) {
                initSportsDictionary();
                boggle = new Boggle(sports);
            }else if(selectGameModeValue.equals("Restaurant")) {
                initRestaurantDictionary();
                boggle = new Boggle(restaurant);
            }

            if(selectGameModeLevel == 0) {
                gamePlayScreen.resetTimeSecond(30);
                boolean b = true;
                while (b) {
                    boggleBoard = new BoggleBoard();
                    wordAnswerset = (HashSet<String>) boggle.boggleSolver(boggleBoard.getChar());
                    if(wordAnswerset.size() ==1 ) {
                        for(String str:wordAnswerset) {
                            if(str.length() ==3) {
                                b = false;
                            }
                        }
                    }
                }
                char[][] board = boggleBoard.getChar();
                Button[] circles = gamePlayScreen.getCirclesArray();
                int count = 0;
                for (int j = 0; j < board.length; j++) {
                    for (int k = 0; k < board[0].length; k++) {
                        circles[count++].setText(String.valueOf(board[j][k]));

                    }
                }
                System.out.println("");
                for (String str :  wordAnswerset) {
                    System.out.println(str);
                }
                gamePlayScreen.setTargetScoreLabel("10");


            }else if(selectGameModeLevel == 1) {
                gamePlayScreen.resetTimeSecond(35);
                boolean b = true;
                while (b) {
                    boggleBoard = new BoggleBoard();
                    wordAnswerset = (HashSet<String>) boggle.boggleSolver(boggleBoard.getChar());
                    if(wordAnswerset.size() ==1 ) {
                        for(String str:wordAnswerset) {
                            if(str.length() == 4) {
                                b = false;
                            }
                        }
                    }
                }
                char[][] board = boggleBoard.getChar();
                Button[] circles = gamePlayScreen.getCirclesArray();
                int count = 0;
                for (int j = 0; j < board.length; j++) {
                    for (int k = 0; k < board[0].length; k++) {
                        circles[count++].setText(String.valueOf(board[j][k]));

                    }
                }
                System.out.println("");
                for (String str :  wordAnswerset) {
                    System.out.println(str);
                }
                gamePlayScreen.setTargetScoreLabel("20");
            }else if(selectGameModeLevel == 2) {
                gamePlayScreen.resetTimeSecond(40);
                boolean b = true;
                while (b) {
                    boggleBoard = new BoggleBoard();
                    wordAnswerset = (HashSet<String>) boggle.boggleSolver(boggleBoard.getChar());
                    if(wordAnswerset.size() ==1 ) {
                        for(String str:wordAnswerset) {
                            if(str.length() == 5) {
                                b = false;
                            }
                        }
                    }
                }
                char[][] board = boggleBoard.getChar();
                Button[] circles = gamePlayScreen.getCirclesArray();
                int count = 0;
                for (int j = 0; j < board.length; j++) {
                    for (int k = 0; k < board[0].length; k++) {
                        circles[count++].setText(String.valueOf(board[j][k]));

                    }
                }
                System.out.println("");
                for (String str :  wordAnswerset) {
                    System.out.println(str);
                }
                gamePlayScreen.setTargetScoreLabel("30");
            } else if(selectGameModeLevel == 3) {
                gamePlayScreen.resetTimeSecond(45);
                boolean b = true;
                while (b) {
                    boggleBoard = new BoggleBoard();
                    wordAnswerset = (HashSet<String>) boggle.boggleSolver(boggleBoard.getChar());
                    if(wordAnswerset.size() ==1 ) {
                        for(String str:wordAnswerset) {
                            if(str.length() == 6) {
                                b = false;
                            }
                        }
                    }
                }
                char[][] board = boggleBoard.getChar();
                Button[] circles = gamePlayScreen.getCirclesArray();
                int count = 0;
                for (int j = 0; j < board.length; j++) {
                    for (int k = 0; k < board[0].length; k++) {
                        circles[count++].setText(String.valueOf(board[j][k]));

                    }
                }
                int score = 0;
                System.out.println("");
                for (String str :  wordAnswerset) {
                    System.out.println(str);
                    if(str.length() == 3) {
                        score = score+10;
                    }else if(str.length() == 4) {
                        score = score+20;
                    }else if(str.length() == 5) {
                        score = score+30;
                    }else if(str.length() == 6) {
                        score = score+40;
                    }else if(str.length() == 7) {
                        score = score+50;
                    }
                }
                gamePlayScreen.setTargetScoreLabel(String.valueOf(score));
            }else if(selectGameModeLevel == 4) {
                gamePlayScreen.resetTimeSecond(50);
                boolean b = true;
                while (b) {
                    boggleBoard = new BoggleBoard();
                    wordAnswerset = (HashSet<String>) boggle.boggleSolver(boggleBoard.getChar());
                    if(wordAnswerset.size() == 2) {
                        b = false;
                    }
                }
                char[][] board = boggleBoard.getChar();
                Button[] circles = gamePlayScreen.getCirclesArray();
                int count = 0;
                for (int j = 0; j < board.length; j++) {
                    for (int k = 0; k < board[0].length; k++) {
                        circles[count++].setText(String.valueOf(board[j][k]));

                    }
                }
                int score = 0;
                System.out.println("");
                for (String str :  wordAnswerset) {
                    System.out.println(str);
                    if(str.length() == 3) {
                        score = score+10;
                    }else if(str.length() == 4) {
                        score = score+20;
                    }else if(str.length() == 5) {
                        score = score+30;
                    }else if(str.length() == 6) {
                        score = score+40;
                    }else if(str.length() == 7) {
                        score = score+50;
                    }
                }
                gamePlayScreen.setTargetScoreLabel(String.valueOf(score));
            } else if(selectGameModeLevel == 5) {
                gamePlayScreen.resetTimeSecond(55);
                boolean b = true;
                while (b) {
                    boggleBoard = new BoggleBoard();
                    wordAnswerset = (HashSet<String>) boggle.boggleSolver(boggleBoard.getChar());
                    if(wordAnswerset.size() == 3) {
                        b = false;
                    }
                }
                char[][] board = boggleBoard.getChar();
                Button[] circles = gamePlayScreen.getCirclesArray();
                int count = 0;
                for (int j = 0; j < board.length; j++) {
                    for (int k = 0; k < board[0].length; k++) {
                        circles[count++].setText(String.valueOf(board[j][k]));

                    }
                }
                int score = 0;
                System.out.println("");
                for (String str :  wordAnswerset) {
                    System.out.println(str);
                    if(str.length() == 3) {
                        score = score+10;
                    }else if(str.length() == 4) {
                        score = score+20;
                    }else if(str.length() == 5) {
                        score = score+30;
                    }else if(str.length() == 6) {
                        score = score+40;
                    }else if(str.length() == 7) {
                        score = score+50;
                    }
                }
                gamePlayScreen.setTargetScoreLabel(String.valueOf(score));
            } else if(selectGameModeLevel == 6) {
                gamePlayScreen.resetTimeSecond(60);
                boolean b = true;
                while (b) {
                    boggleBoard = new BoggleBoard();
                    wordAnswerset = (HashSet<String>) boggle.boggleSolver(boggleBoard.getChar());
                    if(wordAnswerset.size() == 4) {
                        b = false;
                    }
                }
                char[][] board = boggleBoard.getChar();
                Button[] circles = gamePlayScreen.getCirclesArray();
                int count = 0;
                for (int j = 0; j < board.length; j++) {
                    for (int k = 0; k < board[0].length; k++) {
                        circles[count++].setText(String.valueOf(board[j][k]));

                    }
                }
                int score = 0;
                System.out.println("");
                for (String str :  wordAnswerset) {
                    System.out.println(str);
                    if(str.length() == 3) {
                        score = score+10;
                    }else if(str.length() == 4) {
                        score = score+20;
                    }else if(str.length() == 5) {
                        score = score+30;
                    }else if(str.length() == 6) {
                        score = score+40;
                    }else if(str.length() == 7) {
                        score = score+50;
                    }
                }
                gamePlayScreen.setTargetScoreLabel(String.valueOf(score));
            } else if(selectGameModeLevel == 7) {
                gamePlayScreen.resetTimeSecond(65);
                boolean b = true;
                while (b) {
                    boggleBoard = new BoggleBoard();
                    wordAnswerset = (HashSet<String>) boggle.boggleSolver(boggleBoard.getChar());
                    if(wordAnswerset.size() == 5) {
                        b = false;
                    }
                }
                char[][] board = boggleBoard.getChar();
                Button[] circles = gamePlayScreen.getCirclesArray();
                int count = 0;
                for (int j = 0; j < board.length; j++) {
                    for (int k = 0; k < board[0].length; k++) {
                        circles[count++].setText(String.valueOf(board[j][k]));

                    }
                }
                int score = 0;
                System.out.println("");
                for (String str :  wordAnswerset) {
                    System.out.println(str);
                    if(str.length() == 3) {
                        score = score+10;
                    }else if(str.length() == 4) {
                        score = score+20;
                    }else if(str.length() == 5) {
                        score = score+30;
                    }else if(str.length() == 6) {
                        score = score+40;
                    }else if(str.length() == 7) {
                        score = score+50;
                    }
                }
                gamePlayScreen.setTargetScoreLabel(String.valueOf(score));
            }

            setGameState(GameState.STARTED_PLAYING);
        }

        gamePlayScreen.startCountdown(appTemplate);

        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
            char guess = event.getCharacter().charAt(0);
            if(Character.isLetter(guess))
            gamePlayScreen.keyboardTypeLightup(guess);
        });


        appTemplate.getGUI().getPrimaryScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                if(ke.getCode() == KeyCode.ENTER) {

                    ArrayList wordList = (ArrayList) gamePlayScreen.getRealWordList();
                    String word = wordList.toString().replaceAll("[^A-Za-z]+", "");
                    if(wordAnswerset.contains(word)) {
                        wordList.clear();
                        if(word.length() == 3) {
                            gamePlayScreen.addGuessedWordData(new Word(word, 10));
                            gamePlayScreen.setTotalScoreDigitLabel(10);
                        }else if(word.length() == 4) {
                            gamePlayScreen.addGuessedWordData(new Word(word, 20));
                            gamePlayScreen.setTotalScoreDigitLabel(20);
                        }else if(word.length() == 5) {
                            gamePlayScreen.addGuessedWordData(new Word(word, 30));
                            gamePlayScreen.setTotalScoreDigitLabel(30);
                        }else if(word.length() == 6) {
                            gamePlayScreen.addGuessedWordData(new Word(word, 40));
                            gamePlayScreen.setTotalScoreDigitLabel(40);
                        }else if(word.length() == 7) {
                            gamePlayScreen.addGuessedWordData(new Word(word, 50));
                            gamePlayScreen.setTotalScoreDigitLabel(50);
                        }

                        gamePlayScreen.setTableBackgourndBack();

                        if(gamePlayScreen.getTotalScore() == gamePlayScreen.getTargetScore() && gamestate.equals(GameState.STARTED_PLAYING)) {
                            setGameState(GameState.UNINITIALIZED);
                            gamePlayScreen.pauseCountdown();
                            gamePlayScreen.disableNodes();
                            appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
                            appMessageDialogSingleton.show(null, "Congratulations, you have successfully overcome this level.");

                            if(selectGameModeValue.equals("Animals") && selectGameModeLevel < 7){

                                if(selectGameModeLevel+1 == gameData.getAnimalsLevel()) {
                                    gameData.setAnimalsLevel(selectGameModeLevel+2);
                                    realSave();
                                }
                                gamePlayScreen.setPlayNextButtonDisable(false);
                            }else if(selectGameModeValue.equals("Sports") && selectGameModeLevel < 7) {

                                if(selectGameModeLevel+1 == gameData.getSportsLeveL()) {
                                    gameData.setSportsLeveL(selectGameModeLevel+2);
                                    realSave();
                                }
                                gamePlayScreen.setPlayNextButtonDisable(false);
                            }else if(selectGameModeValue.equals("Restaurant") && selectGameModeLevel < 7) {

                                if(selectGameModeLevel+1 == gameData.getRestaurantLevel()) {
                                    gameData.setRestaurantLevel(selectGameModeLevel+2);
                                    realSave();
                                }
                                gamePlayScreen.setPlayNextButtonDisable(false);
                            } else {
                                return;
                            }

                            gamePlayScreen.setStartPlayingButton();
                            gamePlayScreen.setStartButtonDisable(true);

                        }

                    }
                    gamePlayScreen.restartWordType();
                    gamePlayScreen.clearHighlighting();

                }
            }
        });

        if(gamestate.equals(GameState.STARTED_PLAYING)) {
            gamePlayScreen.resumeLetterGrid();

        }
    }

    public void displaySolution() {
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        GamePlayScreen gamePlayScreen = (GamePlayScreen) workspace.getScreen(
                propertyManager.getPropertyValue(GAMEPLAYSCREEN));
        gamePlayScreen.clearGuessedWordData();
        for(String word: wordAnswerset) {
            if(word.length() == 3) {
                gamePlayScreen.addGuessedWordData(new Word(word, 10));
            }else if(word.length() == 4) {
                gamePlayScreen.addGuessedWordData(new Word(word, 20));
            }else if(word.length() == 5) {
                gamePlayScreen.addGuessedWordData(new Word(word, 30));
            }else if(word.length() == 6) {
                gamePlayScreen.addGuessedWordData(new Word(word, 40));
            }else if(word.length() == 7) {
                gamePlayScreen.addGuessedWordData(new Word(word, 50));
            }
        }
    }

    private void realSave() {
        try {
            HashMap hashMap = (HashMap)load();
            String profileName = gameData.getProfileName();
            if(hashMap.containsKey(profileName)) {
                hashMap.remove(profileName);
                hashMap.put(profileName, gameData);
                GameDataHug gameDataHug = (GameDataHug) appTemplate.getDataComponent();
                gameDataHug.setGamedataHugMap(hashMap);
                gameDataHug.addGameData(profileName, gameData);
                save();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void createGamePlayScreen() {
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        GamePlayScreen gamePlayScreen = (GamePlayScreen) workspace.getScreen(
                propertyManager.getPropertyValue(GAMEPLAYSCREEN));
        if(selectGameModeValue != null) {
            gamePlayScreen.setModeNameLabel(selectGameModeValue);
        }

        gamePlayScreen.setLevelLabel(String.valueOf(selectGameModeLevel+1));
        gamePlayScreen.clearEveryNodes();

        AppGUI gui = appTemplate.getGUI();
        gui.setInnerPane(gamePlayScreen.getBorderPane());
        gamePlayScreen.setTableBackgourndBack();
    }

    public void createProfileSettingScreen() {
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        ProfileSettingScreen profileSettingScreen = (ProfileSettingScreen) workspace.getScreen(
                propertyManager.getPropertyValue(PROFILESETTINGSCREEN));
        profileSettingScreen.setActualNameLabel(gameData.getProfileName());
        AppGUI gui = appTemplate.getGUI();
        gui.setInnerPane(profileSettingScreen.getBorderPane());
    }

    @Override
    public void handleSelectModeRequest(int selectedLevel) {
        this.selectGameModeLevel = selectedLevel;
        //load data
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        HomeScreen homeScreen = (HomeScreen) workspace.getScreen(
                propertyManager.getPropertyValue(HOMESCREEN));
        homeScreen.setStartPlayingDisabled(false);
        AppGUI gui = appTemplate.getGUI();
        gui.setInnerPane(homeScreen.getBorderPane());

    }

    public void createLevelSelectionScreen(String selectValue) {
        this.selectGameModeValue = selectValue;
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        LevelSelectionScreen levelSelectionScreen = (LevelSelectionScreen) workspace.getScreen(
                propertyManager.getPropertyValue(LEVELSELECTIONSCREEN));
        levelSelectionScreen.setModeValue(selectValue);
        if(selectGameModeValue.equals("Animals")) {
            levelSelectionScreen.setAvaliableLevel(gameData.getModeLevel("Animals"));
        }else if(selectGameModeValue.equals("Sports")) {
            levelSelectionScreen.setAvaliableLevel(gameData.getModeLevel("Sports"));
        }else if(selectGameModeValue.equals("Restaurant")) {
            levelSelectionScreen.setAvaliableLevel(gameData.getModeLevel("Restaurant"));
        }

        AppGUI gui = appTemplate.getGUI();
        gui.setInnerPane(levelSelectionScreen.getBorderPane());
    }

    @Override
    public void handleHomeButtonRequest() {
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        HomeScreen homeScreen = (HomeScreen) workspace.getScreen(
                propertyManager.getPropertyValue(HOMESCREEN));
        GamePlayScreen gamePlayScreen = (GamePlayScreen) workspace.getScreen(
                propertyManager.getPropertyValue(GAMEPLAYSCREEN));
        homeScreen.setStartPlayingDisabled(true);
        AppGUI gui = appTemplate.getGUI();
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
        if(gamestate.equals(GameState.STARTED_PLAYING)) {
            yesNoCancelDialog.show("", "You are still playing, do you want to leave?");
            if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)){
                gui.setInnerPane(homeScreen.getBorderPane());
                setGameState(GameState.UNINITIALIZED);
                gamePlayScreen.pauseCountdown();
                gamePlayScreen.disableNodes();
                gamePlayScreen.setTimerDigitLabel(null);
                gamePlayScreen.setStartPlayingButton();
                gamePlayScreen.clearEveryNodes();
                gamePlayScreen.restartWordType();
                gamePlayScreen.clearHighlighting();
                gamePlayScreen.setCurrentGuessLabel(null);
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
                selectGameModeLevel=0;
                selectGameModeValue=null;
            }
        } else if(gamestate.equals(GameState.UNINITIALIZED)) {
            gui.setInnerPane(homeScreen.getBorderPane());
            selectGameModeLevel=0;
            selectGameModeValue=null;
        }
        gamePlayScreen.setStartButtonDisable(false);
    }

    @Override
    public void handlePauseButtonRequest() {
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        GamePlayScreen gamePlayScreen = (GamePlayScreen) workspace.getScreen(
                propertyManager.getPropertyValue(GAMEPLAYSCREEN));
        gamePlayScreen.hideLetterGrid();
        gamePlayScreen.setStartPlayingButton();
        gamePlayScreen.pauseCountdown();

    }

    @Override
    public void handleResumeButtonRequest() {

    }

    @Override
    public void handleCreateButtonRequest(String username, String password) {

    }

    @Override
    public void handleLevelButtonRequest() {

    }

    @Override
    public void handleConfirmRequest(String username, String password) {

    }

    @Override
    public void handleModeComboBoxRequest() {

    }

    @Override
    public void handleHelpRequest() {
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        HelpScreen helpScreen = (HelpScreen) workspace.getScreen(
                propertyManager.getPropertyValue(HELPSCREEN));
        AppGUI gui = appTemplate.getGUI();
        gui.setInnerPane(helpScreen.getBorderPane());
    }

    @Override
    public void handleExitRequest() {

    }

    @Override
    public void handleMouseDragRequest() {

    }

    @Override
    public void handleTypeRequest() {

    }

    @Override
    public void selectAndDisplayWord() {

    }

    @Override
    public void handlePlayNextButton() {

    }

    @Override
    public void handleSaveButton() {

    }

    private void save() throws IOException{
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent());
    }

    private Map load() throws IOException, ClassNotFoundException{
       return appTemplate.getFileComponent().loadData(appTemplate.getDataComponent());
    }

    private void initAnimalsDictionary() {
        animals = new Boggle.Trie();
        try {
            FileReader fr = new FileReader("/Users/Archi/Desktop/BuzzwordGame3/Buzzword/resources/words/Animals2.txt");
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                animals.addWord(line.split(":")[0].toLowerCase());
            }
        } catch (Exception e) {
            throw new RuntimeException("Error while reading dictionary");
        }
    }

    private void initSportsDictionary() {
        sports = new Boggle.Trie();
        try {
            FileReader fr = new FileReader("/Users/Archi/Desktop/BuzzwordGame3/Buzzword/resources/words/Sports.txt");
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                sports.addWord(line.split(":")[0].toLowerCase());
            }
        } catch (Exception e) {
            throw new RuntimeException("Error while reading dictionary");
        }
    }

    private void initRestaurantDictionary() {
        restaurant = new Boggle.Trie();
        try {
            FileReader fr = new FileReader("/Users/Archi/Desktop/BuzzwordGame3/Buzzword/resources/words/Restaurant.txt");
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                restaurant.addWord(line.split(":")[0].toLowerCase());
            }
        } catch (Exception e) {
            throw new RuntimeException("Error while reading dictionary");
        }
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    @Override
    public void initData() {

    }

    public int getSelectGameModeLevel() {
        return selectGameModeLevel;
    }

    public void setSelectGameModeLevel(int selectGameModeLevel) {
        this.selectGameModeLevel = selectGameModeLevel;
    }
}
