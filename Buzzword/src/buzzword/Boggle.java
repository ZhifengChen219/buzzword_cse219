package buzzword;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public final class Boggle {


    private Trie trie;

    public Boggle(Trie trie) {
        this.trie = trie;
    }

    public Set<String> boggleSolver(char[][] board) {
        boolean[][] tracker = new boolean[4][4];
        if (board == null) {
            throw new NullPointerException("The matrix cannot be null");
        }
        final Set<String> validWords = new HashSet<>();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                solve(board, i, j, "", validWords, tracker, trie);
            }
        }
        return validWords;
    }

    private void solve(char[][] board, int i, int j, String word, Set<String> validWords, boolean[][] tracker, Trie lexicon) {
        assert board != null;
        assert validWords != null;

        if (lexicon.isWord(word)) { validWords.add(word); }

        if (!lexicon.isPrefix(word)) { return; }

        boolean[][] tmp = deepCopy(tracker);
        tmp[i][j] = true;

        if (0 <= i - 1 && !tmp[i-1][j]){
            solve(board, i-1, j, word + board[i-1][j], validWords, tmp, lexicon);
        }

        if (0 <= j - 1 && !tmp[i][j-1]){
            solve(board, i, j-1, word + board[i][j-1], validWords, tmp, lexicon);
        }
        if (i + 1 < board.length && !tmp[i+1][j]){
            solve(board, i+1, j, word + board[i+1][j], validWords, tmp, lexicon);
        }

        if (j + 1 < board[0].length && !tmp[i][j+1]){
            solve(board, i, j+1, word + board[i][j+1], validWords, tmp, lexicon);
        }

    }


    public static boolean[][] deepCopy(boolean[][] original) {
        if (original == null) {
            return null;
        }

        final boolean[][] result = new boolean[original.length][];
        for (int i = 0; i < original.length; i++) {
         result[i] = Arrays.copyOf(original[i], original[i].length);
        }
        return result;
    }

    public static class Trie {
        //the root only serves to anchor the trie.

        private TrieNode root;

        public Trie() {
            root = new TrieNode('\0');
        }//constructor


        public boolean isPrefix(String word) {
            TrieNode n = root;
            for (char c : word.toCharArray()) {
                n = n.next[c];
                if (null == n) {
                    return false;
                }
            }
            return !n.nextIsEmpty();
        }//isPrefix

        public boolean isWord(String word) {
            TrieNode n = root;
            for (char c : word.toCharArray()) {
                n = n.next[c];
                if (null == n) {
                    return false;
                }
            }
            return n.word;
        }

        public boolean addWord(String word) {
            TrieNode n = root, tmp;
            for (char c : word.toCharArray()) {
                tmp = n.next[c];
                if (tmp == null) {
                    tmp = new TrieNode(c);
                    n.setChild(c, tmp);
                }
                n = tmp;
            }
            if (n.word) {
                return !n.word;
            }
            n.word = true;
            return n.word;
        }//addWord
    }

    public static class TrieNode {

        public char value;
        boolean word = false;
        TrieNode[] next = new TrieNode[256];
        private int nextLength = 0;

        public TrieNode(char c) {
            value = c;
        }

        void setChild(char c, TrieNode node){
            next[c]=node;
            nextLength++;
        }
        void clearNext() {
            next = new TrieNode[256];
            nextLength = 0;
        }

        boolean nextIsEmpty(){
            return nextLength == 0;
        }

        int nextSize(){
            return nextLength;
        }
    }
}