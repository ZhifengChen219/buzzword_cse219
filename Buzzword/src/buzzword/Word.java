package buzzword;

/**
 * Created by Archi on 12/9/16.
 */
public class Word {
    private String word;
    private int score;

    public Word(String word, int score) {
        this.word = word;
        this.score = score;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
