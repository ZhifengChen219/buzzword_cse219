package buzzword;

import apptemplate.AppTemplate;
import components.AppComponentsBuilder;
import components.AppDataComponent;
import components.AppFileComponent;
import components.AppWorkspaceComponent;
import data.GameData;
import data.GameDataFile;
import data.GameDataHug;
import gui.Workspace;

/**
 * Created by Archi on 11/6/16.
 */
public class Buzzword extends AppTemplate{
    public static void main(String[] args) {
        launch(args);
    }


    public String getFileControllerClass() {
        return "BuzzwordController";
    }

    public AppComponentsBuilder makeAppBuilderHook() {
        return new AppComponentsBuilder() {
            @Override
            public AppDataComponent buildDataComponent() throws Exception {
                return GameDataHug.getSingleton();
            }

            @Override
            public AppFileComponent buildFileComponent() throws Exception {
                return new GameDataFile();
            }

            @Override
            public AppWorkspaceComponent buildWorkspaceComponent() throws Exception {
                return new Workspace(Buzzword.this);
            }
        };
    }

}
